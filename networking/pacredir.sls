{% from "networking/map.jinja" import networking with context %}

home_salt_states_networking_pacredir_installed:
  pkg.installed:
    - pkgs: {{ networking.pkgs.pacredir|yaml }}

home_salt_states_networking_pacman_conf_managed:
  file.managed:
    - name: /etc/pacman.conf
    - source: salt://networking/templates/pacman.conf.jinja
    - user: root
    - group: root
    - mode: 644
    - template: jinja
    - require:
      - home_salt_states_networking_pacredir_installed

home_salt_states_networking_pacredir_service_enabled:
  service.running:
    - name: pacserve
    - enable: True
    - require:
      - home_salt_states_networking_pacredir_installed

home_salt_states_networking_pacserve_service_enabled:
  service.running:
    - name: pacserve
    - enable: True
    - require:
      - home_salt_states_networking_pacredir_installed
