{% from "networking/map.jinja" import networking with context %}

home_salt_states_networking_avasarala_persistent_networking_file_managed:
  file.managed:
    - name: /etc/udev/rules.d/70-persistent-net.rules
    - source: salt://networking/files/avasarala/70-persistent-net.rules
    - user: root
    - group: root
    - mode: 644

home_salt_states_networking_avasarala_netplan_bonded_configuration_managed:
  file.managed:
    - name: /etc/netplan/02-netbond.yaml
    - source: salt://networking/files/avasarala/02-netbond.yaml
    - user: root
    - group: root
    - mode: 644

{# Don't do any other configuration than this. It'll reconfigure on the next
reboot, but I don't want to mess about with networking during a state.apply.
That seems kinda dangerous. #}
