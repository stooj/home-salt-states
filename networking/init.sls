{% set os_family = salt['grains.get']('os_family', 'None') %}
include:
  - .tools
  - .networkmanager
  {% if os_family == 'Arch' %}
  - .pacredir
  {% endif %}
