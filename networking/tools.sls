{% from "networking/map.jinja" import networking with context %}

home_salt_states_networking_nmon_installed:
  pkg.installed:
    - pkgs: {{ networking.pkgs.nmon|yaml }}

home_salt_states_networking_bind_tools_installed:
  pkg.installed:
    - pkgs: {{ networking.pkgs.bind_tools|yaml }}
