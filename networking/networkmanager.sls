{% from "networking/map.jinja" import networking with context %}

home_salt_states_networking_network_manager_installed:
  pkg.installed:
    - pkgs: {{ networking.pkgs.networkmanager|yaml }}

home_salt_states_networking_network_manager_service_started:
  service.running:
    - name: NetworkManager
    - enable: True
