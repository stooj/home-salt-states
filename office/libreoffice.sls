{% from "office/map.jinja" import office with context %}

home_salt_states_office_libreoffice_installed:
  pkg.installed:
    - pkgs: {{ office.pkgs.libreoffice|yaml }}
