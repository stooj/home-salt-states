{% from "office/map.jinja" import office with context %}
{% set os = salt['grains.get']('os', None) %}

{% if os == 'Ubuntu' %}
home_salt_states_office_nextcloud_ppa_managed:
  pkgrepo.managed:
    - ppa: nextcloud-devs/client
{% endif %}

home_salt_states_office_nextcloud_client_installed:
  pkg.installed:
    - pkgs: {{ office.pkgs.nextcloud_client|yaml }}
    {% if os == 'Ubuntu' %}
    - requirements:
      - home_salt_states_office_nextcloud_ppa_managed
    {% endif %}
