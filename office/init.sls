include:
  - .calibre
  - .ledger
  - .libreoffice
  - .latex
  - .mumble
  - .nextcloud_client
  - .pandoc
  {# Temporarily disable printing until we get it plugged in
  # {% if salt.grains.get('location', None) == 'innellan' %}
  # - .printing
  # {% endif %}
  #}
  - .workrave
