{% from "office/map.jinja" import office with context %}

home_salt_states_office_cups_installed:
  pkg.installed:
    - pkgs: {{ office.pkgs.cups|yaml }}

home_salt_states_office_cups_enabled:
  service.running:
    - name: org.cups.cupsd.service
    - enable: true
    - require:
      - home_salt_states_office_cups_installed

home_salt_states_office_hp_printer_installed:
  pkg.installed:
    - pkgs: {{ office.pkgs.hp_printer|yaml }}
    - require:
      - home_salt_states_office_cups_installed

home_salt_states_office_hp_printer_configured:
  cmd.run:
    - name: hp-setup --interactive --auto --type=print --printer=HP_OfficeJet_Pro_8710 -x 172.16.7.1
    - creates: /etc/cups/ppd/HP_OfficeJet_Pro_8710.ppd
    - require:
      - home_salt_states_office_hp_printer_installed
