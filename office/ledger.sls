{% from "office/map.jinja" import office with context %}

home_salt_states_office_ledger_installed:
  pkg.installed:
    - pkgs: {{ office.pkgs.ledger|yaml }}

home_salt_states_office_hledger_installed:
  pkg.installed:
    - pkgs: {{ office.pkgs.hledger|yaml }}
