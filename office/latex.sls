{% from "office/map.jinja" import office with context %}

{% for package in [
  'texlive',
  'texstudio',
  'texmaker',
  'lyx'
  ]
%}
home_salt_states_office_{{ package }}_installed:
  pkg.installed:
    - pkgs: {{ office.pkgs.get(package)|yaml }}
{% endfor %}
