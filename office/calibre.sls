{% from "office/map.jinja" import office with context %}

home_salt_states_office_calibre_installed:
  pkg.installed:
    - pkgs: {{ office.pkgs.calibre|yaml }}
