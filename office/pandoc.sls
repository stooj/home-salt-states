{% from "office/map.jinja" import office with context %}

home_salt_states_office_pandoc_installed:
  pkg.installed:
    - pkgs: {{ office.pkgs.pandoc|yaml }}
