{% from "zfs/map.jinja" import zfs with context %}

home_salt_states_zfs_package_installed:
  pkg.installed:
    - pkgs: {{ zfs.pkgs.zfs|yaml }}
