{% from "zfs/map.jinja" import zfs with context %}

{% set pools = zfs.get('pools', {}) %}

{% for pool, data in pools.items() %}
{% if data.config is defined %}
{% set config = data.config %}
{% endif %}
{% if data.properties is defined %}
{% set properties = data.properties %}
{% endif %}
{% if data.filesystem_properties is defined %}
{% set filesystem_properties = data.filesystem_properties %}
{% endif %}
{% set vdevs = data.vdevs %}

home_salt_states_zfs_{{ pool }}_pool:
  zpool.present:
    - name: {{ pool }}
    - require:
      - home_salt_states_zfs_package_installed
    {% if config is defined %}
    - config:
        {% for key, val in config.items() %}
        {{ key }}: {{ val }}
        {% endfor %}
    {% endif %}
    {% if properties is defined %}
    - properties:
        {% for key, val in properties.items() %}
        {{ key }}: {{ val }} 
        {% endfor %}
    {% endif %}
    {% if filesystem_properties is defined %}
    - filesystem_properties:
        {% for key, val in filesystem_properties.items() %}
        {{ key }}: {{ val }}
        {% endfor %}
    {% endif %}
    - layout:
        {% for vdev, disks in vdevs.items() %}
        - {{ vdev }}:
          {% for disk in disks %}
          - {{ disk }}
          {% endfor %}
        {% endfor %}
{% endfor %}

{% set filesystems = zfs.get('filesystems', {}) %}

{% for filesystem, data in filesystems.items() %}
{% set pool = data.pool %}
{% set path = data.path %}
{% set create_parent = data.get('create_parent', True) %}
{% if data.properties is defined %}
{% set properties = data.properties %}
{% endif %}
{% if data.cloned_from is defined %}
{% set cloned_from = data.cloned_from %}
{% endif %}
home_salt_states_zfs_{{ pool }}_{{ filesystem }}_filesystem_present:
  zfs.filesystem_present:
    - name: {{ pool }}/{{ path }}
    - create_parent: {{ create_parent }}
    {% if properties is defined %}
    - properties:
        {% for key, val in properties.items() %}
        {{ key }}: {{ val }} 
        {% endfor %}
    {% endif %}
    {% if cloned_from is defined %}
    - cloned_from: {{ cloned_from }}
    {% endif %}
    - require:
      - home_salt_states_zfs_{{ pool }}_pool

{#
{% set user = data.get('owner', 'root') %}
{% set group = data.get('group', 'root') %}
{% set dir_mode = data.get('dir_mode', 755) %}
{% set file_mode = data.get('file_mode', 644) %}
{% if data.recurse is defined %}
  {% if data.recurse != False %}
    {% set recurse = data.recurse %}
  {% endif %}
{% else %}
  {% set recurse = ['user', 'group', 'mode'] %}
{% endif %}
home_salt_states_zfs_{{ pool }}_{{ filesystem }}_filesystem_ownership:
  file.directory:
    - name: /{{ pool }}/{{ path }}
    - user: {{ user }}
    - group: {{ group }}
    - dir_mode: {{ dir_mode }}
    - file_mode: {{ file_mode }}
    {% if recurse is defined %}
    - recurse:
      {% for item in recurse %}
      - {{ item }}
      {% endfor %}
    {% endif %}
    - require:
      - home_salt_states_zfs_{{ pool }}_{{ filesystem }}_filesystem_present
#}
{% endfor %}
