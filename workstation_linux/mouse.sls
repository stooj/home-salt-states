{% if salt['grains.get']('os', 'None') == 'Ubuntu' %}
workstation_linux_roccat_mouse_repo_managed:
  pkgrepo.managed:
    - humanname: roccat tools
    - name: ppa:berfenger/roccat
    - file: /etc/apt/sources.list.d/roccat.list
    - keyid: F038E545
    - keyserver: keyserver.ubuntu.com

workstation_linux_roccat_tools_installed:
  pkg.installed:
    - name: roccat-tools

workstation_linux_roccat_group_has_users:
  group.present:
    - name: roccat
    - addusers:
{% for username, user in salt.pillar.get('users', {}).items() if (username != 'root' and (user.absent is not defined or not user.absent)) %}
      - {{ username }}
{% endfor %}
{% endif %}
