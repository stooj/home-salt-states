{% from "workstation_linux/map.jinja" import workstation_linux with context %}

workstation_linux_bluetooth_installed:
  pkg.installed:
    - pkgs: [bluez, bluez-utils]

workstation_linux_bluetooth_service_enabled:
  service.running:
    - name: bluetooth
    - enable: True
    - require:
      - workstation_linux_bluetooth_installed
