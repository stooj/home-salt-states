{% from "workstation_linux/map.jinja" import workstation_linux with context %}
{% set os_family = salt.grains.get('os_family', None) %}

{% set hostname = salt.grains.get('id', None) %}
{% if hostname == 'anfang.stooj.org' %}
{% else %}
linux_workstation_nvidia_driver_installed:
  pkg.installed:
    - pkgs: {{ workstation_linux.nvidia|yaml }}

{% if os_family == 'Arch' %}
linux_workstation_nvidia_32bit_driver_installed:
  pkg.installed:
    - pkgs: {{ workstation_linux.nvidia_32|yaml }}
    - require:
      - home_salt_states_networking_pacman_conf_managed
{% endif %}
{% endif %}
