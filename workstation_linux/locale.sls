workstation_linux_gb_locale:
  locale.present:
    - name: en_GB.UTF-8

workstation_linux_us_locale:
  locale.present:
    - name: en_US.UTF-8

workstation_linux_default_locale:
  locale.system:
    - name: en_GB.UTF-8
    - require:
      - locale: workstation_linux_gb_locale
