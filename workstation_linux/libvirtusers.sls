{% if salt['grains.get']('os_family', None) == 'Arch' %}
workstation_linux_libvirt_group_has_users:
  group.present:
    - name: libvirt
    - addusers:
      - stooj

{% endif %}
