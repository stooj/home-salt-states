workstation_linux_timezone_configuration:
  timezone.system:
    - name: Europe/London
    - utc: True

workstation_linux_systemd_ntp_service_managed:
  service.running:
    - name: systemd-timesyncd
    - enable: True
