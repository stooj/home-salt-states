{% from "workstation_linux/map.jinja" import workstation_linux with context %}
{% set os_family = salt.grains.get('os_family', None) %}

{% if os_family == 'Arch' %}
linux_workstation_intel_drivers_installed:
  pkg.installed:
    - pkgs: {{ workstation_linux.intel|yaml }}

linux_workstation_intel_32bit_drivers_installed:
  pkg.installed:
    - pkgs: {{ workstation_linux.intel_32|yaml }}
    - require:
      - linux_workstation_intel_drivers_installed
      - home_salt_states_networking_pacman_conf_managed

linux_workstation_intel_udev_rule_managed:
  file.managed:
    - name: /etc/udev/rules.d/90-backlight.rules
    - source: salt://workstation_linux/files/intel/90-backlight.rules
    - user: root
    - group: root
    - mode: 0644
{% endif %}
