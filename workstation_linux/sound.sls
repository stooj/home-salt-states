{% from "workstation_linux/map.jinja" import workstation_linux with context %}
{# Disabled while I investigate network congestion

workstation_linux_sound_esound_protocol_enabled:
  file.uncomment:
    - name: /etc/pulse/default.pa
    - regex: load-module module-esound-protocol-tcp

workstation_linux_sound_native_protocol_enabled:
  file.uncomment:
    - name: /etc/pulse/default.pa
    - regex: load-module module-native-protocol-tcp

workstation_linux_sound_zeroconf_publish_enabled:
  file.uncomment:
    - name: /etc/pulse/default.pa
    - regex: load-module module-zeroconf-publish

workstation_linux_sound_rtp_receiever_enabled:
  file.uncomment:
    - name: /etc/pulse/default.pa
    - regex: load-module module-rtp-recv

workstation_linux_sound_rtp_sender_null_sink_enabled:
  file.uncomment:
    - name: /etc/pulse/default.pa
    - regex: load-module module-null-sink sink_name=rtp format=s16be channels=2 rate=44100 sink_properties="device.description='RTP Multicast Sink'"

workstation_linux_sound_rtp_send_enabled:
  file.uncomment:
    - name: /etc/pulse/default.pa
    - regex: load-module module-rtp-send source=rtp.monitor

#}
