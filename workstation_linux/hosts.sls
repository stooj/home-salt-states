{% from "workstation_linux/map.jinja" import workstation_linux with context %}
{% set hostname = salt['grains.get']('host') %}
workstation_linux_localhost_points_to_hostname:
  host.only:
    - name: 127.0.1.1
    - hostnames:
      - {{ hostname }}
      - {{ hostname }}.localdomain

workstation_linux_pris_host_entry_removed:
  host.absent:
    - name: pris
    - ip: 192.168.1.8

workstation_linux_pris_localdomain_host_entry_removed:
  host.absent:
    - name: pris.localdomain
    - ip: 192.168.1.8

{% set hosts = workstation_linux.get('hosts', {}) %}

{% for address, machines in hosts.items() %}
{% if hostname in machines %}
{% else %}
workstation_linux_{{ address }}_host_entry_present:
  host.only:
    - name: {{ address }}
    - hostnames:
      {% for host in machines %}
      - {{ host }}
      {% endfor %}
{% endif %}
{% endfor %}
