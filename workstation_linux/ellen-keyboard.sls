{% if salt['grains.get']('os_family', 'None') == 'Debian' %}

{# TODO: Use keyboard grain to determine us/uk keyboard #}
workstation_linux_default_keyboard_reconfigured:
  debconf.set:
    - name: keyboard-configuration
    - data:
        "keyboard-configuration/layoutcode": {'type': "string", 'value': "us"}
        "keyboard-configuration/switch": {'type': "select", 'value': "No temporary switch"}
        "keyboard-configuration/toggle": {'type': "select", 'value': "No toggling"}
        "console-setup/detect": {'type': "detect-keyboard", 'value': ""}
        "keyboard-configuration/unsupported_options": {'type': "boolean", 'value': "true"}
        "keyboard-configuration/model": {'type': "select", 'value': "Microsoft Natural Ergonomic 4000"}
        "console-setup/ask_detect": {'type': "boolean", 'value': "false"}
        "console-setup/detected": {'type': "note", 'value': ""}
        "keyboard-configuration/optionscode": {'type': "string", 'value': "ctrl:swapcaps"}
        "keyboard-configuration/xkb-keymap": {'type': "select", 'value': ""}
        "keyboard-configuration/modelcode": {'type': "string", 'value': "microsoft4000"}
        "keyboard-configuration/altgr": {'type': "select", 'value': "The default for the keyboard layout"}
        "keyboard-configuration/unsupported_config_layout": {'type': "boolean", 'value': "true"}
        "keyboard-configuration/store_defaults_in_debconf_db": {'type': "boolean", 'value': "true"}
        "keyboard-configuration/unsupported_config_options": {'type': "boolean", 'value': "true"}
        "keyboard-configuration/variant": {'type': "select", 'value': "English (US)"}
        "keyboard-configuration/compose": {'type': "select", 'value': "No compose key"}
        "keyboard-configuration/layout": {'type': "select", 'value': "English (US)"}
        "keyboard-configuration/variantcode": {'type': "string", 'value': ""}
        "keyboard-configuration/unsupported_layout": {'type': "boolean", 'value': "true"}
        "keyboard-configuration/ctrl_alt_bksp": {'type': "boolean", 'value': "true"}
{% endif %}
