{% from "workstation_linux/map.jinja" import workstation_linux with context %}

{% if salt.grains.get('os', 'None') == 'Ubuntu' %}
{% set codename = salt.grains.get('oscodename', 'bionic') %}
{% for humanname, name in [
    ('main', 'http://gb.archive.ubuntu.com/ubuntu/ ' ~ codename ~ ' main restricted'),
    ('updates', 'http://gb.archive.ubuntu.com/ubuntu/ ' ~ codename ~ '-updates main restricted'),
    ('universe', 'http://gb.archive.ubuntu.com/ubuntu/ ' ~ codename ~ ' universe'),
    ('universe-updates', 'http://gb.archive.ubuntu.com/ubuntu/ ' ~ codename ~ '-updates universe'),
    ('multiverse', 'http://gb.archive.ubuntu.com/ubuntu/ ' ~ codename ~ ' multiverse'),
    ('multiverse-updates', 'http://gb.archive.ubuntu.com/ubuntu/ ' ~ codename ~ '-updates multiverse'),
    ('backports', 'http://gb.archive.ubuntu.com/ubuntu/ ' ~ codename ~ '-backports main restricted universe multiverse'),
    ('security', 'http://security.ubuntu.com/ubuntu ' ~ codename ~ '-security main restricted'),
    ('security-universe', 'http://security.ubuntu.com/ubuntu ' ~ codename ~ '-security universe'),
    ('security-multiverse', 'http://security.ubuntu.com/ubuntu ' ~ codename ~ '-security multiverse')
  ]
%}
workstation_linux_uncomment_default_{{ humanname }}_repo:
  pkgrepo.managed:
    - humanname: {{ humanname }}
    - name: deb {{ name }}

workstation_linux_uncomment_default_{{ humanname }}_src_repo:
  pkgrepo.managed:
    - humanname: {{ humanname }}
    - name: deb-src {{ name }}
    {% if loop.last -%}
    - refresh: True
    {%- endif %}
{% endfor %}
{% endif %}

{% for key, package in workstation_linux.pkgs.items() %}
workstation_linux_packages_{{ key }}_installed:
  pkg.installed:
    - name: {{ package }}
{% endfor %}
