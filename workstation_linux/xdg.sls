{% from "workstation_linux/map.jinja" import workstation_linux with context %}

workstation_linux_xdg_dirs_defaults_managed:
  file.managed:
    - name: /etc/xdg/user-dirs.defaults
    - source: salt://workstation_linux/files/xdg/user-dirs.defaults
    - user: root
    - group: root
    - mode: 0644
