include:
  - .timezone
  - .hosts
  - .locale
  - .packages
  - .keyboard
  - .sound
  - .xdg
  - .mouse
  - .libvirtusers
  - .bluetooth
  {% if salt['grains.get']('chassis', None) == 'laptop' %}
  - .laptop
  {% endif %}
