{% from "workstation_linux/map.jinja" import workstation_linux with context %}
{% set wireless_psk = salt.pillar.get('workstation_linux:wireless_psk', '') %}

workstation_linux_tlp_installed:
  pkg.installed:
    - name: tlp

workstation_linux_tlp_service_managed:
  service.running:
    - name: tlp
    - enable: True

workstation_linux_acpi_installed:
  pkg.installed:
    - name: acpi

workstation_linux_acpilight_installed:
  pkg.installed:
    - name: acpilight

{% if salt['grains.get']('os_family', None) == 'Arch' %}
workstation_linux_video_group_exists:
  group.present:
    - name: video
{% endif %}

workstation_linux_empire_network_managed:
  file.managed:
    - name: /etc/NetworkManager/system-connections/EMPIRE.nmconnection
    - source: salt://workstation_linux/templates/laptop/EMPIRE.nmconnection.jinja
    - template: jinja
    - user: root
    - group: root
    - mode: 600
    - context:
        wifi_psk: {{ wireless_psk }}
