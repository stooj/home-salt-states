base:
  '*':
    - salt.minion
  "P@os:(Debian|Ubuntu|RedHat):":
    {% if salt['grains.get']('osrelease', 'blocked') in [
      'blocked',
      '17.04',
      '17.10',
      '18.04',
      ]
    %}
    - salt.pkgrepo.absent
    {% else %}
    - salt.pkgrepo
  {% endif %}
  "G@virtual:KVM or G@virtual:physical or G@virtual:kvm":
    - common_linux
    - mounts
    - users
    - users.sudo
    - openssh
    - openssh.client
    - openssh.config
  {#
  'G@has_data_drive:True':
    - mounts.workstation
  #}
  'G@has_sshds:True':
    - common_linux.fstrim
  'G@zfs:True':
    - zfs
  'G@role:saltmaster':
    - salt.master
    - salt.cloud
    - salt.gitfs.keys
    - saltmaster-additional-rules
  'G@chassis:server':
    - server_linux
  avasarala.ginstoo.net:
    - networking.avasarala
  'G@role:goodteith-migration':
    - goodteith.migration
  'G@role:personal-computer':
    - android
    - arduino
    - chat
    - fonts
    - games
    - gnome.gdm
    - gnome.gnome_keyring
    - graphics
    - i3wm
    - keybase
    - libvirt
    - muttmail
    - networking
    - nfs.client
    - office
    - plasma.everything
    - programming
    - programming.gui
    - ranger
    - simplehelp.technician
    - sound
    - video
    - virtualization
    - wire
    - workstation_linux
    - xorg
    - yubikey
  "G@role:personal-computer and G@osfamily:Debian":
    - packages.snaps
  'G@role:cio-laptop':
    - chat.weechat
    - fonts
    - gnome.gdm
    - gnome.gnome_keyring
    - graphics
    - i3wm
    - libvirt
    - muttmail
    - networking
    - nfs.client
    - nfs.mount
    - office.libreoffice
    - office.latex
    - office.pandoc
    - office.workrave
    - plasma.everything
    - programming.aws
    - programming.python
    - programming.go
    - programming.tools
    - ranger
    - sound.mpc
    - sound.pulseaudio
    - video.mpv
    - virtualization
    - workstation_linux
    - xorg
    - yubikey
  'ellen.stooj.org':
    - workstation_linux.ellen-keyboard
  'lucy.stooj.org':
    - workstation_linux.lucy-keyboard
  'naomi.ginstoo.net':
    - wireguard.client
  'gpus:model:^(GM|GF)':
    - match: grain_pcre
    - workstation_linux.nvidia
  'gpus:model:^(HD)':
    - match: grain_pcre
    - workstation_linux.intel
  'G@role:kvm-host':
    - kvm-host
  'G@role:bind':
    - bind
    - bind.config
  'G@role:simplehelp-server':
    - simplehelp
  "G@role:docker-server or G@roles:docker-server":
    - docker
    {% for container in salt.grains.get('docker_containers', []) %}
    - docker_containers.{{ container }}
    {% endfor %}
  "G@role:wireguard-server or G@roles:wireguard-server":
    - wireguard.package
    - wireguard.server
  "G@role:nfs-server or G@roles:nfs-server":
    - nfs.server
  'G@role:paperless-server':
    - paperless
  "G@role:media-server or G@roles:media-server":
    - mpd
    - mpd.music_server_config
  'G@role:zabbix-server':
    - zabbix.agent.repo
    - zabbix.agent.conf

    - mysql.server.conf
    - mysql.client.conf
    - zabbix.mysql.conf
    - zabbix.mysql.schema

    - zabbix.server.repo
    - zabbix.server.conf

    - nginx.conf

    - php.fpm.repo
    - php.fpm.conf
    - php.fpm.mysql
    - php.fpm.bcmath
    - php.fpm.mbstring
    - php.fpm.gd
    - php.fpm.xml
    - php.fpm.opcache

    - zabbix.frontend.repo
    - zabbix.frontend.conf
