{% from "mpd/map.jinja" import mpd with context %}

home_salt_states_mpd_global_config_managed:
  file.managed:
    - name: /etc/mpd.conf
    - source: salt://mpd/templates/mpd.conf.jinja
    - template: jinja
    - user: root
    - group: root
    - mode: 644
    - context:
        config: {{ mpd.config|yaml }}

home_salt_states_mpd_global_service_managed:
  service.running:
    - name: mpd
    - enable: True
    - watch:
      - home_salt_states_mpd_global_config_managed
