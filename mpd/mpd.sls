{% from "mpd/map.jinja" import mpd with context %}

home_salt_states_mpd_mpd_installed:
  pkg.installed:
    - pkgs: {{ mpd.pkgs.mpd|yaml }}
