{% from "ranger/map.jinja" import ranger with context %}

home_salt_states_ranger_ranger_installed:
  pkg.installed:
    - pkgs: {{ ranger.pkgs.ranger|yaml }}
