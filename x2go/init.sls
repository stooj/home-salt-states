{% from "x2go/map.jinja" import x2go with context %}
{% set os = salt.grains.get('os', None) %}

{% if os == 'Ubuntu' %}
home_salt_states_x2go_ppa_managed:
  pkgrepo.managed:
    - ppa: x2go/stable
{% endif %}

home_salt_states_x2go_server_installed:
  pkg.installed:
    - pkgs: {{ x2go.pkgs.x2go|yaml }}
    {% if os == 'Ubuntu' %}
    - requirements:
      - home_salt_states_x2go_ppa_managed
    {% endif %}
