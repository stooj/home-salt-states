{% from "video/map.jinja" import video with context %}

home_salt_states_video_vlc_installed:
  pkg.installed:
    - pkgs: {{ video.pkgs.vlc|yaml }}
