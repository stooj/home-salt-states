{% from "video/map.jinja" import video with context %}

home_salt_states_dvd_playback_installed:
  pkg.installed:
    - pkgs: {{ video.pkgs.dvd_playback|yaml }}

{# TODO

On Ubuntu, you need to use dpkg-reconfigure to set up dvd playback.

See https://help.ubuntu.com/community/RestrictedFormats/PlayingDVDs

Will need to set dpkg selections to get it to install

#}
