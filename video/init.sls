include:
  - .mpv
  - .vlc
  {% if salt['grains.get']('has_optical', None) == True %}
  - .dvd_playback
  {% endif %}
