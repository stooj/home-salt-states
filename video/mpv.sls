{% from "video/map.jinja" import video with context %}

home_salt_states_video_mpv_installed:
  pkg.installed:
    - pkgs: {{ video.pkgs.mpv|yaml }}

home_salt_states_video_mpv_config_managed:
  file.managed:
    - name: /etc/mpv/mpv.conf
    - source: salt://video/templates/mpv/mpv.conf.jinja
    - template: jinja
    - user: root
    - group: root
    - mode: 644
