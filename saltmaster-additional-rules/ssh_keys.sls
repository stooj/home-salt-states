salt_master_ssh_key_available_in_fileserver:
  file.symlink:
    - target: /etc/salt/pki/gitfs/global.key
    - name: /srv/salt/ssh/files/deploy_key
    - makedirs: True
    - user: root
    - group: root
    - mode: 660
