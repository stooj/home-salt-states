{% from "mounts/map.jinja" import mounts with context %}

{# There are many dependencies with managing directories, so I've decided to
   put them all in the same spot.

   This is ordered into four groups:
   - bind mounts
   - directories
   - absent
   - symlinks
   - autofs

   Bind mounts are mounted filesystems from one directory to another.

   Directories are managed directories.

   Absent are directories that should *not* exist on the filesystem

   Symlinks are managed symlinks

   Autofs rules are purely for NFS autodiscovery. See
   https://wiki.archlinux.org/index.php/Autofs#NFS_network_mounts

   Each group has two types: user and system. A system entry will be created
   once, whereas a user entry will be created for each user.

   The user type is default.

   All pillar data for user entries will be run through a parser, which will
   replace the string "username" with the correct user.

   The owner will default to the current user.
   The group will default the the current user.
   createtarget will default to True
   force will default to False

   Autofs mounts do not support user types

   Pillar data should look like this:

   mounts:
     bind:
       videos:
         name: /home/username/videos
         target: /srv/videos/username
         user: username
         group: users
         createtarget: True  # If the target directory needs to be created as well
         type: user
     directories:
       backup:
         name: /home/username/backup
       documents:
         name: /home/username/documents
         permissions: 2755
     absent:
       Rubbish:
         name: /home/username/Rubbish
     symlinks:
       Downloads:
         name: /home/username/Downloads
         target: /home/username/downloads
         force: True
     autofs:
       mnt:
         name: /mnt
         timeout: 60  # Defaults to 60
   #}


{% if mounts.autofs is defined %}
home_salt_states_mounts_autofs_installed:
  pkg.installed:
    - pkgs: {{ mounts.pkgs.autofs|yaml }}

home_salt_states_mounts_autofs_enabled:
  service.running:
    - name: autofs
    - enable: True
{% endif %}

{% set bindmounts = mounts.get('bind', {}) %}
{% set directorymounts = mounts.get('directories', {}) %}
{% set absentmounts = mounts.get('absent', {}) %}
{% set symlinkmounts = mounts.get('symlinks', {}) %}
{% set autofsmounts = mounts.get('autofs', {}) %}

{% for bind, data in bindmounts.items() %}
  {% if data.type is defined %}
    {% set type = data.type %}
  {% else %}
    {% set type = 'user' %}
  {% endif %}
  {% if data.createtarget is defined %}
    {% set createtarget = data.createtarget %}
  {% else %}
    {% set createtarget = True %}
  {% endif %}

  {% if type == 'user' %}
    {% for username, user in salt.pillar.get('users', {}).items() if (username != 'root' and (user.absent is not defined or not user.absent)) %}
      {% set name = data.name|replace('username', username) %}
      {% set target = data.target|replace('username', username) %}
      {% if data.user is defined %}
        {% set user = data.user|replace('username', username) %}
      {% else %}
        {% set user = username %}
      {% endif %}
      {% if data.group is defined %}
        {% set group = data.group|replace('username', username) %}
      {% else %}
        {% set group = username %}
      {% endif %}
      {% if data.dir_mode is defined %}
        {% set dir_mode = data.dir_mode %}
      {% else %}
        {% set dir_mode = '0755' %}
      {% endif %}
home_salt_states_bind_mounts_{{ bind }}_{{ username }}_mount_point_exists:
  file.directory:
    - name: {{ name }}
    - user: {{ user }}
    - group: {{ group }}
    - makedirs: True
    - dir_mode: {{ dir_mode }}
    - require:
      - user: {{ user }}
      - group: {{ group }}

{% if createtarget %}
home_salt_states_bind_mounts_{{ bind }}_{{ username }}_target_point_exists:
  file.directory:
    - name: {{ target }}
    - user: {{ user }}
    - group: {{ group }}
    - dir_mode: {{ dir_mode }}
    - require:
      - user: {{ user }}
      - group: {{ group }}
{% endif %}

home_salt_states_bind_mounts_{{ bind }}_{{ username }}_mounted:
  mount.mounted:
    - name: {{ name }}
    - device: {{ target }}
    - fstype: none
    - opts: bind
    - dump: 0
    - pass_num: 0
    - persist: True
    - mkmnt: False
    - require:
      - home_salt_states_bind_mounts_{{ bind }}_{{ username }}_mount_point_exists
      {% if createtarget %}
      - home_salt_states_bind_mounts_{{ bind }}_{{ username }}_target_point_exists
      {% endif %}
{% endfor %}
{% else %}
{% set name = data.name %}
{% if data.user is defined %}
{% set user = data.user %}
{% else %}
{% set user = 'root' %}
{% endif %}
{% if data.group is defined %}
{% set group = data.group %}
{% else %}
{% set group = 'root' %}
{% endif %}
{% if data.dir_mode is defined %}
{% set dir_mode = data.dir_mode %}
{% else %}
{% set dir_mode = '0755' %}
{% endif %}
home_salt_states_bind_mounts_{{ bind }}_mount_point_exists:
  file.directory:
    - name: {{ name }}
    - user: {{ user }}
    - group: {{ group }}
    - dir_mode: {{ dir_mode }}
    - require:
      - user: {{ user }}
      - group: {{ group }}

{% if createtarget %}
home_salt_states_bind_mounts_{{ bind }}_target_point_exists:
  file.directory:
    - name: {{ target }}
    - user: {{ user }}
    - group: {{ group }}
    - dir_mode: {{ dir_mode }}
    - require:
      - user: {{ user }}
      - group: {{ group }}
{% endif %}

home_salt_states_bind_mounts_{{ bind }}_mounted:
  mount.mounted:
    - name: {{ name }}
    - device: {{ target }}
    - fstype: none
    - opts: bind
    - dump: 0
    - pass_num: 0
    - persist: True
    - mkmnt: False
    - require:
      - home_salt_states_bind_mounts_{{ bind }}_mount_point_exists
      {% if createtarget %}
      - home_salt_states_bind_mounts_{{ bind }}_target_point_exists
      {% endif %}
{% endif %}
{% endfor %}

{% for directory, data in directorymounts.items() %}
  {% if data.type is defined %}
    {% set type = data.type %}
  {% else %}
    {% set type = 'user' %}
  {% endif %}

  {% if type == 'user' %}
    {% for username, user in salt.pillar.get('users', {}).items() if (username != 'root' and (user.absent is not defined or not user.absent)) %}
      {% set name = data.name|replace('username', username) %}
      {% if data.user is defined %}
        {% set user = data.user|replace('username', username) %}
      {% else %}
        {% set user = username %}
      {% endif %}
      {% if data.group is defined %}
        {% set group = data.group|replace('username', username) %}
      {% else %}
        {% set group = username %}
      {% endif %}
      {% if data.dir_mode is defined %}
        {% set dir_mode = data.dir_mode %}
      {% else %}
        {% set dir_mode = '0755' %}
      {% endif %}
home_salt_states_directory_mounts_{{ directory }}_{{ username }}_managed:
  file.directory:
    - name: {{ name }}
    - user: {{ user }}
    - group: {{ group }}
    - dir_mode: {{ dir_mode }}
    - require:
      - user: {{ user }}
      - group: {{ group }}

    {% endfor %}
  {% else %}
    {% set name = data.name %}
    {% if data.user is defined %}
      {% set user = data.user %}
    {% else %}
      {% set user = 'root' %}
    {% endif %}
    {% if data.group is defined %}
      {% set group = data.group %}
    {% else %}
      {% set group = 'root' %}
    {% endif %}
    {% if data.dir_mode is defined %}
      {% set dir_mode = data.dir_mode %}
    {% else %}
      {% set dir_mode = '0755' %}
    {% endif %}
home_salt_states_directory_mounts_{{ directory }}_managed:
  file.directory:
    - name: {{ name }}
    - user: {{ user }}
    - group: {{ group }}
    - dir_mode: {{ dir_mode }}
    - require:
      - user: {{ user }}
      - group: {{ group }}
  {% endif %}
{% endfor %}

{% for absent, data in absentmounts.items() %}
  {% if data.type is defined %}
    {% set type = data.type %}
  {% else %}
    {% set type = 'user' %}
  {% endif %}

  {% if type == 'user' %}
    {% for username, user in salt.pillar.get('users', {}).items() if (username != 'root' and (user.absent is not defined or not user.absent)) %}
      {% set name = data.name|replace('username', username) %}
home_salt_states_absent_mounts_{{ absent }}_{{ username }}_not_present:
  file.absent:
    - name: {{ name }}
    {% endfor %}
  {% else %}
home_salt_states_absent_mounts_{{ absent }}_not_present:
  file.absent:
    - name: {{ name }}
  {% endif %}
{% endfor %}

{% for symlink, data in symlinkmounts.items() %}
  {% if data.type is defined %}
    {% set type = data.type %}
  {% else %}
    {% set type = 'user' %}
  {% endif %}

  {% if type == 'user' %}
    {% for username, user in salt.pillar.get('users', {}).items() if (username != 'root' and (user.absent is not defined or not user.absent)) %}
      {% set name = data.name|replace('username', username) %}
      {% set target = data.target|replace('username', username) %}
      {% if data.user is defined %}
        {% set user = data.user|replace('username', username) %}
      {% else %}
        {% set user = username %}
      {% endif %}
      {% if data.group is defined %}
        {% set group = data.group|replace('username', username) %}
      {% else %}
        {% set group = username %}
      {% endif %}
      {% if data.force is defined %}
        {% set force = data.force %}
      {% else %}
        {% set force = False %}
      {% endif %}
home_salt_states_symlink_mounts_{{ symlink }}_{{ username }}_symlink_exists:
  file.symlink:
    - name: {{ name }}
    - target: {{ target }}
    - force: {{ force }}
    - user: {{ user }}
    - group: {{ group }}
    - require:
      - user: {{ user }}
      - group: {{ group }}
    {% endfor %}
  {% else %}
    {% set name = data.name %}
    {% set target = data.target %}
    {% if data.user is defined %}
      {% set user = data.user %}
    {% else %}
      {% set user = 'root' %}
    {% endif %}
    {% if data.group is defined %}
      {% set group = data.group|replace('username', username) %}
    {% else %}
      {% set group = 'root' %}
    {% endif %}
    {% if data.force is defined %}
      {% set force = data.force %}
    {% else %}
      {% set force = False %}
    {% endif %}
home_salt_states_symlink_mounts_{{ symlink }}_symlink_exists:
  file.symlink:
    - name: {{ name }}
    - target: {{ target }}
    - force: {{ force }}
    - user: {{ user }}
    - group: {{ group }}
    - require:
      - user: {{ user }}
      - group: {{ group }}
  {% endif %}
{% endfor %}

{% for autofs, data in autofsmounts.items() %}
  {% if data.name is defined %}
  {% set name = data.name %}
  {% else %}
  {% set name = autofs %}
  {% endif %}
  {% if data.mountpoint is defined %}
  {% set mountpoint = data.mountpoint %}
  {% else %}
  {% set mountpoint = '/mnt' %}
  {% endif %}
  {% set server = data.server %}
  {% set share_location = data.share_location %}
  {% if data.timeout is defined %}
  {% set timeout = data.timeout %}
  {% else %}
  {% set timeout = 60 %}
  {% endif %}
  {% if mount_options is defined %}
  {% set mount_options = data.mount_options %}
  {% else %}
  {% set mount_options = '-rw,soft,intr,rsize=8192,wsize=8192' %}
  {% endif %}
home_salt_states_mounts_autofs_salt_{{ server }}_{{ name }}_{{ mountpoint }}_mounts_added_to_master_conf:
  file.append:
    - name: {{ mounts.paths.autofs_conf_location ~ 'master' }}
    - text: {{ mountpoint }} {{ mounts.paths.autofs_conf_location ~ server }} --timeout {{ timeout }}
    - require:
      - home_salt_states_mounts_autofs_installed

home_salt_states_autofs_config_{{ server }}_{{ name }}_added:
  file.append:
    - name: {{ mounts.paths.autofs_conf_location ~ server }}
    - text: {{ name }} {{ mount_options }} {{ server }}:{{ share_location }}
    - require:
      - home_salt_states_mounts_autofs_installed
    {% if loop.last %}
    - watch_in: 
      - home_salt_states_mounts_autofs_enabled
    {% endif %}
{% endfor %}

{#
home_salt_state_mounts_mounts_test_file_managed:
  file.managed:
    - name: /tmp/mountsdata
    - contents: {{ mounts }}

home_salt_state_bind_mounts_test_file_managed:
  file.managed:
    - name: /tmp/binddata
    - contents: {{ bindmounts }}

home_salt_state_directory_mounts_test_file_managed:
  file.managed:
    - name: /tmp/directorydata
    - contents: {{ directorymounts }}

home_salt_state_absent_mounts_test_file_managed:
  file.managed:
    - name: /tmp/absentdata
    - contents: {{ absentmounts }}

home_salt_state_symlink_mounts_test_file_managed:
  file.managed:
    - name: /tmp/symlinkdata
    - contents: {{ symlinkmounts }}

home_salt_state_pillar_mounts_test_file_managed:
  file.managed:
    - name: /tmp/pillardata
    - contents_pillar: mounts

{% set tempdata = salt['pillar.get']('mounts') %}
home_salt_state_pillar_direct_mounts_test_file_managed:
  file.managed:
    - name: /tmp/pillardirectdata
    - contents: {{ tempdata }}
    #}
