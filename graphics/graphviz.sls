{% from "graphics/map.jinja" import graphics with context %}

home_salt_states_graphics_graphviz_installed:
  pkg.installed:
    - pkgs: {{ graphics.pkgs.graphviz|yaml }}

{# Zopfli is an alternative gzip compressor that improves on gzip output sizes. It is very good, but slow.
 # It comes with a script to compress png images, suitable for website assets.
 # See 
 # - https://blog.codinghorror.com/zopfli-optimization-literally-free-bandwidth/
 # - https://github.com/google/zopfli
 #}
home_salt_states_graphics_zopfli_installed:
  pkg.installed:
    - pkgs: {{ graphics.pkgs.zopfli|yaml }}
