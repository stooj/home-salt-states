include:
  - .gimp
  - .inkscape
  - .darktable
  - .imagemagick
  - .graphviz
  - .pdf-tools
  - .scanner
