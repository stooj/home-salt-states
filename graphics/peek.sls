{% from "graphics/map.jinja" import graphics with context %}

{% set os_family = salt.grains.get('os_family', None) %}

{% if os_family == 'Debian' %}
home_salt_sates_graphics_peek_ppa_managed:
  pkgrepo.managed:
    - ppa: peek-developers/stable
{% endif %}

home_salt_states_graphics_peek_installed:
  pkg.installed:
    - pkgs: {{ graphics.pkgs.peek|yaml }}
  {% if os_family == 'Debian' %}
  - require:
    - home_salt_sates_graphics_peek_ppa_managed
  {% endif %}
