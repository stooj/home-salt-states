{% from "graphics/map.jinja" import graphics with context %}

{% if salt['grains.get']('os_family', 'None') == 'Debian' %}
home_salt_states_graphics_flpsed_installed:
  pkg.installed:
    - pkgs: {{ graphics.pkgs.flpsed|yaml }}
{% endif %}
