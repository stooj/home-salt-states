{% from "graphics/map.jinja" import graphics with context %}

home_salt_states_graphics_gimp_installed:
  pkg.installed:
    - pkgs: {{ graphics.pkgs.gimp|yaml }}
