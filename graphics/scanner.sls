{% from "graphics/map.jinja" import graphics with context %}

home_salt_states_graphics_scanner_installed:
  pkg.installed:
    - pkgs: {{ graphics.pkgs.scanner|yaml }}
