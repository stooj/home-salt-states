{% from "graphics/map.jinja" import graphics with context %}

home_salt_states_graphics_darktable_installed:
  pkg.installed:
    - pkgs: {{ graphics.pkgs.darktable|yaml }}


home_salt_states_graphics_gphoto_installed:
  pkg.installed:
    - pkgs: {{ graphics.pkgs.gphoto2|yaml }}
