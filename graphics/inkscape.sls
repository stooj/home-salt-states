{% from "graphics/map.jinja" import graphics with context %}

home_salt_states_graphics_inkscape_installed:
  pkg.installed:
    - pkgs: {{ graphics.pkgs.inkscape|yaml }}
