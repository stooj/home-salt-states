{% from "xorg/map.jinja" import xorg with context %}

home_salt_states_xorg_video_installed:
  pkg.installed:
    - pkgs: {{ xorg.pkgs.video|yaml }}

