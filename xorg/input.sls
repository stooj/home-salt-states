{% from "xorg/map.jinja" import xorg with context %}

home_salt_states_xorg_input_installed:
  pkg.installed:
    - pkgs: {{ xorg.pkgs.input|yaml }}
