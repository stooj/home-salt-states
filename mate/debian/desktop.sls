{% set os_family = salt['grains.get']('os_family', 'None') %}
{% from "mate/" ~ os_family.lower() ~ "/map.jinja" import mate with context %}

{% for group in [
  'accessibility',
  'alsa',
  'archiving',
  'bc',
  'cron',
  'drivers',
  'fonts',
  'gnome',
  'localization',
  'mate',
  'media',
  'networking',
  'peripherals',
  'printing',
  'security',
  'software',
  'virtualization',
  'xdg',
  'xorg'
]
%}
home_salt_states_mate_{{ group }}_desktop_installed:
  pkg.installed:
    - pkgs: {{ mate.pkgs.desktop.get(group)|yaml }}
{% endfor %}
