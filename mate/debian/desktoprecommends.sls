{% set os_family = salt['grains.get']('os_family', 'None') %}
{% from "mate/" ~ os_family.lower() ~ "/map.jinja" import mate with context %}

{% for group in [
  'accessibility',
  'archiving',
  'backup',
  'compiz',
  'debugging',
  'fonts',
  'mate',
  'media',
  'networking',
  'office',
  'peripherals',
  'power_management',
  'printing',
  'security',
  'software',
  'theming',
  'web',
  'xdg',
  'xorg'
%}
home_salt_states_mate_{{ group }}_desktop_recommends_installed:
  pkg.installed:
    - pkgs: {{ mate.pkgs.desktop.get(group)|yaml }}
{% endfor %}
