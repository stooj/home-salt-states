# TODO

## Install

## Snaps to keep an eye out for

## Aur packages

- python-cookiecutter
- ruby-pry
- python-proselint
- python-notify2
- android-sdk-platform-tools
- lgogdownloader
- flpsed
- lbdb
- imapfilter
- courier-maildrop ?
- tiny-media-manager
- rofi-pass
- ttf-emojione
    (post-install: sudo cp /etc/fonts/conf.{avail,d}/75-emojione.conf)
- ruby-rdoc
- todotxt
- Private internet access package
  - Copy `~/pia.sh` to `~/bin/pia.sh`
- Postgres for dentally (9.6)
- heroku-cli
- perl-extract-url-git
- system76-driver
- makemkv
- lua-cjson

## Ruby versions

- Make sure rbenv has a local copy of ruby2.3.3. Check for the files and run the
    installer if they are missing. In Arch, you need openssl-1.0 and build it
    with the following env variable:
    ``PKG_CONFIG_PATH=/usr/lib/openssl-1.0/pkgconfig rbenv install 2.3.3``

## Unknown arch packages!

- ledger-py

## Pacman

[pacman - ArchWiki](https://wiki.archlinux.org/index.php/Pacman#Cleaning_the_package_cache)
- Install pacman-contrib
- Enable paccache.timer

## Mooltipass

Getting the correct version of mooltipass is a bit tricky...

1. We need to get the latest release data from github:

```bash
curl https://api.github.com/repos/mooltipass/moolticute/releases/latest
```

2. Find the correct release, probably using the following key:

```json
"content_type": "build-linux/deb/moolticute_0.20.2_amd64.deb: application/vnd.debian.binary-package",
```

3. For that release, retrieve the url:

```json
"url": "https://api.github.com/repos/mooltipass/moolticute/releases/assets/8104569",
```

4. Download that deb and install it
5. Ensure you don't overwrite existing installations

## Networking

- Install `networkmanager-openvpn`

## Other

- xsel not installed on arch
- gitgutter icons not displaying correctly on arch
- font not correct for arch urxvt (see gitgutter for example)
- [ctags not detected properly](https://github.com/xolox/vim-easytags/issues/168)

### .profile

- `.profile` uses `ksshaskpass` for servers. That's daft. Change it.
- `$HOME/bin` seems to have moved to `$HOME/.local/bin` - update `$PATH`
    accordingly
- Don't bother setting `$TERMINAL` for servers in `.profile`

### bashrc

- A looot of env vars that I really probably don't need

### Bin files

- Don't need `mount-dentally-buckets`

### vim

- Don't need outdoors and indoors files for servers
Actually, this whole setup needs updated, trimmed and tidied

### SSH

- Probably some keys that can be removed there

## Avasarala

- Port forwarding for internal irc, then send all irc traffic to avasarala, who
    should hand it on to pris. https://www.digitalocean.com/community/tutorials/how-to-forward-ports-through-a-linux-gateway-with-iptables
  (Unless you get to Matrix on Avasarala first)
- Set up blog and migrate to avasarala
    (https://dtucker.co.uk/hack/docker-letsencrypt-pelican.html)
