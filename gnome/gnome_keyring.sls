{% from "gnome/map.jinja" import gnome with context %}

home_salt_states_gnome_gnome_keyring_installed:
  pkg.installed:
    - pkgs: {{ gnome.pkgs.gnome_keyring|yaml }}

home_salt_states_gnome_seahorse_installed:
  pkg.installed:
    - pkgs: {{ gnome.pkgs.seahorse|yaml }}
