{% from "gnome/map.jinja" import gnome with context %}

home_salt_states_gnome_gdm_installed:
  pkg.installed:
    - pkgs: {{ gnome.pkgs.gdm|yaml }}

home_salt_states_gnome_gdm_enabled:
  service.running:
    - name: gdm
    - enable: True
    - no_block: True

{% for username, user in salt.pillar.get('users', {}).items() if (username != 'root' and (user.absent is not defined or not user.absent)) %}
{% set email = user.get('email', 'mail@example.org') %}
{% set desktop_environment = user.get('desktop_environment', 'i3') %}
{% set keyboard_layout = user.get('keyboard_layout', 'gb') %}
{% if user.profile_picture is defined %}
  {% set picture_configured = True %}
{% else %}
  {% set picture_configured = False %}
{% endif %}
home_salt_states_gnome_gdm_{{ username }}_profile_managed:
  file.managed:
    - name: /var/lib/AccountsService/users/{{ username }}
    - source: salt://gnome/templates/gdm/user.jinja
    - template: jinja
    - context:
        username: {{ username }}
        email: {{ email }}
        desktop_environment: {{ desktop_environment }}
        keyboard_layout: {{ keyboard_layout }}
        picture_configured: {{ picture_configured }}

{% if picture_configured %}
{% set picture_source = user.profile_picture.source %}
{% set picture_hash = user.profile_picture.source_hash %}
home_salt_states_gnome_gdm_{{ username }}_profile_picture_managed:
  file.managed:
    - name: /var/lib/AccountsService/icons/{{ username }}.png
    - source: {{ picture_source }}
    - source_hash: {{ picture_hash }}
{% endif %}
{% endfor %}
