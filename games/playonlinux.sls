{% from "games/map.jinja" import games with context %}

{% set osfamily = salt['grains.get']('os_family', 'None') %}

{% if osfamily == 'Arch' %}
home_salt_states_games_playonlinux_netcat_installed:
  pkg.installed:
    - pkgs: {{ games.pkgs.netcat|yaml }}
{% endif %}

home_salt_states_games_playonlinux_installed:
  pkg.installed:
    - pkgs: {{ games.pkgs.playonlinux|yaml }}
    {% if osfamily == 'Arch' %}
    - require:
      - home_salt_states_games_playonlinux_netcat_installed
    {% endif %}
