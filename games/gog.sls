{% from "games/map.jinja" import games with context %}

{% if salt['grains.get']('os_family', 'None') == 'Debian' %}
home_salt_states_games_lgogdownloader_installed:
  pkg.installed:
    - pkgs: {{ games.pkgs.lgogdownloader|yaml }}
{% endif %}
