{% from "games/map.jinja" import games with context %}

home_salt_states_games_native_sgt_puzzles_installed:
  pkg.installed:
    - pkgs: {{ games.pkgs.sgt_puzzles|yaml }}
