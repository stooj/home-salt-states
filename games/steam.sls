{% from "games/map.jinja" import games with context %}

{% set os_family = salt['grains.get']('os_family', 'None') %}

home_salt_states_games_steam_installed:
  pkg.installed:
    - pkgs: {{ games.pkgs.steam|yaml }}
    {% if os_family == 'Arch' %}
    - refresh: True
    - require:
      - home_salt_states_networking_pacman_conf_managed
    {% endif %}
