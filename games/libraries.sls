{% from "games/map.jinja" import games with context %}

home_salt_states_games_libraries_installed:
  pkg.installed:
    - pkgs: {{ games.pkgs.libraries|yaml }}
