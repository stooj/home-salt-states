{% from "games/map.jinja" import games with context %}

{% if salt['grains.get']('os_family', None) == 'Debian' %}
{% set osrelease = salt['grains.get']('osrelease', '16.04') %}
{% set osfullname = salt['grains.get']('osfullname', 'None') %}
home_salt_states_games_lutris_repo_managed:
  pkgrepo.managed:
    - humanname: Lutris
    {% if osfullname == 'Debian' %}
    - name: deb http://download.opensuse.org/repositories/home:/strycore/Debian_{{ osrelease }} ./
    - key_url: http://download.opensuse.org/repositories/home:/strycore/Debian_{{ osrelease }}/Release.key 
    {% elif osfullname == 'Ubuntu' %}
    - name: deb http://download.opensuse.org/repositories/home:/strycore/xUbuntu_{{ osrelease }} ./
    - key_url: http://download.opensuse.org/repositories/home:/strycore/xUbuntu_{{ osrelease }}/Release.key 
    {% endif %}
{% endif %}

home_salt_states_games_lutris_installed:
  pkg.installed:
    - pkgs: {{ games.pkgs.lutris|yaml }}
    {% if salt['grains.get']('os_family', 'None') == 'Debian' %}
    - require:
      - home_salt_states_games_lutris_repo_managed
    {% endif %}
