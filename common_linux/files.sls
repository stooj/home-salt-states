{%- for username, user in salt['pillar.get']('users', {}).items() if (user.absent is not defined or not user.absent) -%}
{%- set current = salt.user.info(username) -%}
{%- set user_home = salt['pillar.get'](('users:' ~ username ~ ':home'), current.get('home', '/home/' ~ username )) -%}
common_linux_install_salt_script_{{ username }}_not_installed:
  file.absent:
    - name: {{ user_home }}/install_salt.sh
{% endfor %}
