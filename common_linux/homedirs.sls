{%- for username, user in salt['pillar.get']('users', {}).items() if (user.absent is not defined or not user.absent) -%}
{%- set current = salt.user.info(username) -%}
{%- set user_home = salt['pillar.get'](('users:' ~ username ~ ':home'), current.get('home', '/home/' ~ username )) -%}
{% if user.dotfiles is defined %}
{% if user.dotfiles %}
{% if loop.first %}
include:
{% endif %}
  - {{ username }}_dotfiles
{% endif %}
{% endif %}
{% endfor %}
