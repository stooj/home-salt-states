{% from "common_linux/map.jinja" import common_linux with context %}

{% for key, package in common_linux.pkgs.items() %}
common_linux_packages_{{ key }}_installed:
  pkg.installed:
    - name: {{ package }}
{% endfor %}

common_update_packages:
  pkg.uptodate:
    - refresh: True

