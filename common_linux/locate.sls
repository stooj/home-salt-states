{% if salt['grains.get']('os_family', None) == 'Arch' %}
common_linux_locate_timer_enabled:
  service.running:
    - name: updatedb.timer
    - enable: True
    - require:
      - common_linux_packages_mlocate_installed
{% endif %}
