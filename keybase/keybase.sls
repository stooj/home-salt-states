{% from "keybase/map.jinja" import keybase with context %}

{% set osfamily = salt.grains.get('os_family', None) %}
{% if osfamily == 'Arch' %}
home_salt_states_keybase_keybase_installed:
  pkg.installed:
    - pkgs: {{ keybase.pkgs.keybase|yaml }}
{% endif %}
