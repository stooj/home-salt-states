{% from "programming/map.jinja" import programming with context %}

{# We need to ensure that ruby is installed on the system first #}
home_salt_states_programming_ruby_installed:
  pkg.installed:
    - pkgs: {{ programming.pkgs.ruby|yaml }}

{#
   Pry is an alternative to the ruby command line, with plugins, syntax
   highlighting, source and documentation browsing.
   See http://pryrepl.org/
#}
{% if salt['grains.get']('os_family', 'None') == 'Debian' %}
home_salt_states_programming_pry_installed:
  pkg.installed:
    - pkgs: {{ programming.pkgs.pry|yaml }}
{% endif %}
