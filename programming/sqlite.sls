{% from "programming/map.jinja" import programming with context %}

home_salt_states_programming_sqlite_installed:
  pkg.installed:
    - pkgs: {{ programming.pkgs.sqlite|yaml }}

home_salt_states_programming_sqlite_browser_installed:
  pkg.installed:
    - pkgs: {{ programming.pkgs.sqlitebrowser|yaml }}

{% if salt['grains.get']('os_family', 'None') == 'Debian' %}
home_salt_states_programming_sqlite_dev_headers_installed:
  pkg.installed:
    - pkgs: {{ programming.pkgs.sqlite_dev|yaml }}
{% endif %}
