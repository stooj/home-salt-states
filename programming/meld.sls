{% from "programming/map.jinja" import programming with context %}

home_salt_states_programming_meld_installed:
  pkg.installed:
    - pkgs: {{ programming.pkgs.meld|yaml }}
