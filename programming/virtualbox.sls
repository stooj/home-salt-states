{% from "programming/map.jinja" import programming with context %}

home_salt_states_programming_virtualbox_installed:
  pkg.installed:
    - pkgs: {{ programming.pkgs.virtualbox|yaml }}

home_salt_states_programming_virtualbox_guest_additions_installed:
  pkg.installed:
    - pkgs: {{ programming.pkgs.virtualbox_guest_additions|yaml }}

{% for username, user in salt['pillar.get']('users', {}).items() if (user.absent is not defined or not user.absent) %}
{% set home = "/home/%s" % username %}
home_salt_states_programming_virtualbox_default_machine_location_{{ username }}_set:
  cmd.run:
    - name: vboxmanage setproperty machinefolder {{ home }}/.virtualbox_vms
    - runas: {{ username }}
    - require:
      - user: {{ username }}
      - home_salt_states_programming_virtualbox_installed
    - unless:
      - vboxmanage list systemproperties | grep folder | grep {{ home }}/.virtualbox_vms 
{% endfor %}
