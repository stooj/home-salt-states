{% from "programming/map.jinja" import programming with context %}

home_salt_states_programming_dostounix_installed:
  pkg.installed:
    - pkgs: {{ programming.pkgs.dostounix|yaml }}

home_salt_states_programming_tig_installed:
  pkg.installed:
    - pkgs: {{ programming.pkgs.tig|yaml }}

home_salt_states_programming_tk_installed:
  pkg.installed:
    - pkgs: {{ programming.pkgs.tk|yaml }}

home_salt_states_programming_the_silver_searcher_installed:
  pkg.installed:
    - pkgs: {{ programming.pkgs.the_silver_searcher|yaml }}

home_salt_states_programming_ack_installed:
  pkg.installed:
    - pkgs: {{ programming.pkgs.ack|yaml }}

home_salt_states_programming_gdb_installed:
  pkg.installed:
    - pkgs: {{ programming.pkgs.gdb|yaml }}

{% set os_family = salt.grains.get('os_family', None) %}
{% if os_family == 'Arch' %}
home_salt_states_programming_ripgrep_installed:
  pkg.installed:
    - pkgs: {{ programming.pkgs.ripgrep|yaml }}

home_salt_states_programming_fzf_installed:
  pkg.installed:
    - pkgs: {{ programming.pkgs.fzf|yaml }}
{% endif %}
