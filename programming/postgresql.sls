{% from "programming/map.jinja" import programming with context %}

home_salt_states_programming_postgresql_installed:
  pkg.installed:
    - pkgs: {{ programming.pkgs.postgresql|yaml }}
