{% from "programming/map.jinja" import programming with context %}

{% if salt['grains.get']('os_family', 'None') == 'Debian' %}
home_salt_states_programming_vagrant_repo_enabled:
  pkgrepo.managed:
    - humanname: Wolfgangs Vagrant Repo
    - name: deb https://vagrant-deb.linestarve.com any main
    - keyid: AD319E0F7CFFA38B4D9F6E55CE3F3DE92099F7A4 
    - keyserver: keyserver.ubuntu.com
    - require_in:
      - home_salt_states_programming_vagrant_installed
{% endif %}

home_salt_states_programming_vagrant_installed:
  pkg.installed:
    - pkgs: {{ programming.pkgs.vagrant|yaml }}

home_salt_states_programming_packer_installed:
  pkg.installed:
    - pkgs: {{ programming.pkgs.packer|yaml }}

{% if salt['grains.get']('os_family', 'None') == 'Debian' %}
home_salt_states_programming_vagrant_libvirt_installed:
  pkg.installed:
    - pkgs: {{ programming.pkgs.vagrant_libvirt|yaml }}
{# TODO: Install vagrant plugin on arch #}
{% endif %}

home_salt_states_programming_vagrant_sudo_rules_enabled:
  file.managed:
    - name: /etc/sudoers.d/vagrant-syncedfolders
    - source: salt://programming/templates/vagrant/sudo_rules.jinja
    - template: jinja
    - user: root
    - group: root
    - mode: 440

home_salt_states_programming_vagrant_rdesktop_installed:
  pkg.installed:
    - pkgs: {{ programming.pkgs.rdesktop|yaml }}
