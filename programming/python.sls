{% from "programming/map.jinja" import programming with context %}

home_salt_states_programming_jedi_installed:
  pkg.installed:
    - pkgs: {{ programming.pkgs.jedi|yaml }}

home_salt_states_programming_python_pip_installed:
  pkg.installed:
    - pkgs: {{ programming.pkgs.pip|yaml }}

home_salt_states_programming_python_3_pip_installed:
  pkg.installed:
    - pkgs: {{ programming.pkgs.pip3|yaml }}
