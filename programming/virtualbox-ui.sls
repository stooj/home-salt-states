{% from "programming/map.jinja" import programming with context %}

{% if salt['grains.get']('os_family', 'None') == 'Debian' %}
home_salt_states_programming_virtualbox_ui_installed:
  pkg.installed:
    - pkgs: {{ programming.pkgs.virtualbox_ui|yaml }}
{% endif %}
