{% from "wire/map.jinja" import wire with context %}

{% set os_family = salt.grains.get('os_family', None) %}

{% if os_family == 'Debian' %}
home_salt_states_wire_desktop_repo_managed:
  pkgrepo.managed:
    - humanname: wiredesktop
    - name:  deb https://wire-app.wire.com/linux/debian stable main
    - key_url: https://wire-app.wire.com/linux/releases.key
{% endif %}

home_salt_states_chat_wire_installed:
  pkg.installed:
    - pkgs: {{ wire.pkgs.wire|yaml }}
    {% if os_family == 'Debian' %}
    - require:
      - home_salt_states_wire_desktop_repo_managed
    {% endif %}
