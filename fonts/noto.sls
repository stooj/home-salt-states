{% from "fonts/map.jinja" import fonts with context %}

home_salt_states_fonts_noto_font_installed:
  pkg.installed:
    - pkgs: {{ fonts.pkgs.noto|yaml }}
