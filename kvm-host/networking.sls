eno1:
  network.managed:
    - enabled: True
    - type: eth
    - proto: manual
    - bridge: br0

br0:
  network.managed:
    - enabled: True
    - type: bridge
    - proto: static
    - ports: eno1
    - ipaddr: 192.168.0.7
    - gateway: 192.168.0.1
    - netmask: 255.255.255.0
    - dns:
      - 8.8.8.8
      - 8.8.4.4
    - use:
      - network: eno1
    - require:
      - network: eno1
