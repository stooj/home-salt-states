fs.inotify.max_queued_events:
  sysctl.present:
    - value: 1048576

fs.inotify.max_user_instances:
  sysctl.present:
    - value: 1048576

fs.inotify.max_user_watches:
  sysctl.present:
    - value: 1048576

file_limits_file_for_lxc_multiple_containers:
  file.managed:
    - name: /etc/security/limits.d/lxc_limits.conf
    - source: salt://kvm-host/files/etc/security/limits.d/lxc_limits.conf
    - user: root
    - group: root
    - mode: 644
    - makedirs: True
