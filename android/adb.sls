{% from "android/map.jinja" import android with context %}

{% if salt['grains.get']('os_family', 'None') == 'Debian' %}
home_salt_states_android_adb_installed:
  pkg.installed:
    - pkgs: {{ android.pkgs.adb|yaml }}
{% endif %}
