{% from "android/map.jinja" import android with context %}

{% if salt['grains.get']('os_family', 'None') == 'Debian' %}
home_salt_states_android_fastboot_installed:
  pkg.installed:
    - pkgs: {{ android.pkgs.fastboot|yaml }}
{% endif %}
