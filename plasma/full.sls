{% set osfamily = salt['grains.get']('os_family', 'None') %}
include:
  - .{{ osfamily.lower() }}.full
