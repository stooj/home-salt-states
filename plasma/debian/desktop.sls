{% set os_family = salt['grains.get']('os_family', 'None') %}
{% from "plasma/" ~ os_family.lower() ~ "/map.jinja" import plasma with context %}

{% for group in [
  'alsa',
  'archiving',
  'bc',
  'cron',
  'drivers',
  'fonts',
  'localization',
  'media',
  'networking',
  'peripherals',
  'plasma',
  'power_management',
  'printing',
  'security',
  'software',
  'theming',
  'virtualization',
  'xdg',
  'xorg',
  ]
%}
home_salt_states_plasma_{{ group }}_desktop_installed:
  pkg.installed:
    - pkgs: {{ plasma.pkgs.desktop.get(group)|yaml }}
{% endfor %}
