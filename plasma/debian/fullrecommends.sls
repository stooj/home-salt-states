{% set os_family = salt['grains.get']('os_family', 'None') %}
{% from "plasma/" ~ os_family.lower() ~ "/map.jinja" import plasma with context %}

include:
  - .full

{% for group in [
  'archiving',
  'debugging',
  'drivers',
  'fonts',
  'media',
  'networking',
  'office',
  'peripherals',
  'plasma',
  'power_management',
  'printing',
  'security',
  'software',
  'theming',
  'web',
  'xdg',
  'xorg',
  ]
%}
home_salt_states_plasma_{{ group }}_full_recommends_installed:
  pkg.installed:
    - pkgs: {{ plasma.pkgs.full_recommends.get(group)|yaml }}
{% endfor %}
