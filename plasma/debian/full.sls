{% set os_family = salt['grains.get']('os_family', 'None') %}
{% from "plasma/" ~ os_family.lower() ~ "/map.jinja" import plasma with context %}

include:
  - .desktop

{% for group in [
  'peripherals',
  'plasma',
  'power_management',
  'security',
  'software',
  'theming',
  'xdg',
  ]
%}
home_salt_states_plasma_{{ group }}_full_installed:
  pkg.installed:
    - pkgs: {{ plasma.pkgs.full.get(group)|yaml }}
{% endfor %}
