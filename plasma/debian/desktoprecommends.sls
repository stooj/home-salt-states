{% set os_family = salt['grains.get']('os_family', 'None') %}
{% from "plasma/" ~ os_family.lower() ~ "/map.jinja" import plasma with context %}

include:
  - .desktop

{% for group in [
  'archiving',
  'debugging',
  'drivers',
  'fonts',
  'media',
  'networking',
  'office',
  'peripherals',
  'plasma',
  'power_management',
  'printing',
  'security',
  'software',
  'theming',
  'web',
  'xdg',
  'xorg',
  ]
%}
home_salt_states_plasma_{{ group }}_desktop_recommends_installed:
  pkg.installed:
    - pkgs: {{ plasma.pkgs.desktop_recommends.get(group)|yaml }}
{% endfor %}
