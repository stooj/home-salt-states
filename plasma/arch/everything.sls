{% set os_family = salt['grains.get']('os_family', 'None') %}
{% from "plasma/" ~  os_family.lower()  ~ "/map.jinja" import plasma with context %}

include:
  - .desktop
  - .full
