# Plasma desktop

## Available states

- plasma - Install plasma desktop
- plasma.desktoprecommends - Install plasma desktop and recommended packages for
  the desktop installation (Debian only)
- plasma.full - Install plasma desktop and extras
- plasma.fullrecommends - Install plasma-full recommended packages for the full
  installation (Debian only)
- plasma.everything - All of the above.

Note that plasma.fullrecommends does **not** install everything. 
