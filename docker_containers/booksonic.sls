{% from "docker_containers/map.jinja" import docker_containers with context %}

{% set mountpoint = docker_containers.mountpoint %}

home_salt_states_booksonic_container_running:
  docker_container.running:
    - name: booksonic
    - image: linuxserver/booksonic:latest
    - restart: always
    - binds:
      - {{ mountpoint }}/docker/booksonic/config:/config
      - {{ mountpoint }}/shares/audio/audiobooks:/audiobooks
      - {{ mountpoint }}/shares/audio/podcasts:/podcasts
    - port_bindings:
      - 4041:4040
    - environment:
      - PUID: 1000
      - PGID: 100
      - TZ: Europe/London
    - labels:
      - "traefik.enable=true"
      - "traefik.http.middlewares.booksonic-redirect-websecure.redirectscheme.scheme=https"
      - "traefik.http.routers.booksonic-web.middlewares=booksonic-redirect-websecure"
      - "traefik.http.routers.booksonic-web.rule=Host(`booksonic.ginstoo.net`)"
      - "traefik.http.routers.booksonic-web.entrypoints=web"
      - "traefik.http.routers.booksonic-websecure.rule=Host(`booksonic.ginstoo.net`)"
      - "traefik.http.routers.booksonic-websecure.tls.certresolver=letsencrypt"
      - "traefik.http.routers.booksonic-websecure.tls=true"
      - "traefik.http.routers.booksonic-websecure.entrypoints=websecure"

{% for dir in [
  'shares/audio/audiobooks',
  'shares/audio/podcasts'
  ]
%}
home_salt_states_booksonic_{{ dir|replace('/', '_') }}_managed:
  file.directory:
    - name: {{ mountpoint }}/{{ dir }}
    - user: stooj
    - group: users
    - dir_mode: 0755
    - file_mode: 0644
    - makedirs: True
    - recurse:
      - mode
      - user
      - group
{% endfor %}
{% for dir in [
  'docker/booksonic/config',
  ]
%}
home_salt_states_booksonic_{{ dir|replace('/', '_') }}_managed:
  file.directory:
    - name: {{ mountpoint }}/{{ dir }}
    - user: stooj
    - group: users
    - dir_mode: 0755
    - file_mode: 0644
    - makedirs: True
{% endfor %}
