{% for image in [
  'postgres:10',
  'nextcloud:16-fpm',
  'jrcs/letsencrypt-nginx-proxy-companion',
  'nginx',
  'jwilder/nginx-proxy:alpine',
  ]
%}
home_salt_states_nextcloud_{{ image|replace('/', '_')|replace('-', '_') }}_docker_image_present:
  docker_image.present:
    {% if ':' in image %}
    {% set image = image.split(':', 1) %}
    - name: {{ image[0] }}
    - tag: {{ image[1] }}
    {% else %}
    - name: {{ image }}
    {% endif %}
    - force: true
    - require:
      - sls: docker.install
{% endfor %}

{% set hostname = salt.grains.get('id', None) %}
{% if hostname %}
{% set postgres_password = salt.pillar.get('docker_config:' ~  hostname ~ ':postgres_password') %}
{% else %}
{% set postgres_password = 'test' %}
{% endif %}
{% set virtual_host = salt.pillar.get('docker_config:' ~ hostname ~ ':virtual_host', 'nextcloud.example.com') %}
{% set email = salt.pillar.get('docker_config:' ~ hostname ~ ':email', 'test@example.com') %}
{% set nextcloud_data_directory = salt.pillar.get('docker_config:' ~ hostname ~ ':data_directory', None) %}
{% set nextcloud_admin_user = salt.pillar.get('docker_config:' ~ hostname ~ ':admin_user', None) %}
{% set nextcloud_admin_password = salt.pillar.get('docker_config:' ~ hostname ~ ':admin_password', None) %}

{% if nextcloud_data_directory %}
home_salt_states_nextcloud_data_directory_created:
  file.directory:
    - name: {{ nextcloud_data_directory }}/nextcloud
    - user: www-data
    - group: root
    - dir_mode: 755
{% endif %}

home_salt_states_nextcloud_network_present:
  docker_network.present:
    - name: nextcloud1

home_salt_states_nextcloud_postgres_container_running:
  docker_container.running:
    - name: postgres
    - image: postgres:10
    - restart: always
    - binds:
      - /srv/docker/postgres:/var/lib/postgresql/data
    - environment:
      - POSTGRES_PASSWORD: {{ postgres_password }}
      - POSTGRES_DB: nextcloud
      - POSTGRES_USER: nextcloud
    - networks:
      - nextcloud1
    - require:
      - docker_network: nextcloud1

home_salt_states_nextcloud_nextcloud_container_running:
  docker_container.running:
    - name: nextcloud
    - image: nextcloud:16-fpm
    - restart: always
    - binds:
      - /srv/docker/nextcloud:/var/www/html
      {% if nextcloud_data_directory %}
      - {{ nextcloud_data_directory }}/nextcloud:/var/www/html/data
      {% endif %}
    - environment:
      {# - POSTGRES_HOST: {{ salt.docker.inspect('postgres').get('NetworkSettings').get('IPAddress') }} #}
      - POSTGRES_HOST: postgres
      - POSTGRES_PASSWORD: {{ postgres_password }}
      - POSTGRES_DB: nextcloud
      - POSTGRES_USER: nextcloud
    - networks:
      - nextcloud1
    - require:
      - home_salt_states_nextcloud_postgres_container_running
      - docker_network: nextcloud1
      {% if nextcloud_data_directory %}
      - home_salt_states_nextcloud_data_directory_created
      {% endif %}

{% if salt.docker.exists('nextcloud') %}
{% set ip_address = salt.docker.inspect('nextcloud').get('NetworkSettings').get('IPAddress') %}
{% else %}
{% set ip_address = '127.0.0.1' %}
{% endif %}
home_salt_states_nextcloud_nginx_config_managed:
  file.managed:
    - name: /srv/docker/nginx/nextcloud/nginx.conf
    - source: salt://docker_containers/templates/nextcloud/nginx/nginx.conf
    - template: jinja
    - user: root
    - group: root
    - mode: 644
    - makedirs: True
    - context:
        ip_address: nextcloud
    - networks:
      - nextcloud1
    - require:
      - docker_network: nextcloud1
      - home_salt_states_nextcloud_postgres_container_running

home_salt_states_nextcloud_proxy_uploadsize_config_managed:
  file.managed:
    - name: /srv/docker-files/proxy/uploadsize.conf
    - source: salt://docker_containers/files/nextcloud/proxy/uploadsize.conf
    - user: root
    - group: root
    - mode: 644
    - makedirs: True
    - networks:
      - nextcloud1
    - require:
      - docker_network: nextcloud1

home_salt_states_nextcloud_proxy_dockerfile_managed:
  file.managed:
    - name: /srv/docker-files/proxy/Dockerfile
    - source: salt://docker_containers/files/nextcloud/proxy/Dockerfile
    - user: root
    - group: root
    - mode: 644
    - makedirs: True

home_salt_states_nextcloud_proxy_custom_image_present:
  docker_image.present:
    - name: stooj/proxylargefiles
    - build: /srv/docker-files/proxy
    - tag: latest
    - require:
      - home_salt_states_nextcloud_proxy_uploadsize_config_managed
      - home_salt_states_nextcloud_proxy_dockerfile_managed

home_salt_states_nextcloud_proxy_container_running:
  docker_container.running:
    - name: proxy
    - image: stooj/proxylargefiles
    - tag: latest
    - restart: always
    - port_bindings:
      - 80:80
      - 443:443
    - labels:
      - com.github.jrcs.letsencrypt_nginx_proxy_companion.nginx_proxy: "true"
    - binds:
      - /srv/docker/certs:/etc/nginx/certs:ro
      - /srv/docker/vhost.d:/etc/nginx/vhost.d
      - /srv/docker/html:/usr/share/nginx/html
      - /var/run/docker.sock:/tmp/docker.sock:ro
    - networks:
      - nextcloud1
    - require:
      - docker_network: nextcloud1
      - home_salt_states_nextcloud_proxy_custom_image_present

home_salt_states_nextcloud_letsencrypt_companion_container_running:
  docker_container.running:
    - name: letsencrypt-companion
    - image: jrcs/letsencrypt-nginx-proxy-companion
    - restart: always
    - binds:
      - /srv/docker/certs:/etc/nginx/certs
      - /srv/docker/vhost.d:/etc/nginx/vhost.d
      - /srv/docker/html:/usr/share/nginx/html
      - /var/run/docker.sock:/var/run/docker.sock:ro
    - networks:
      - nextcloud1
    - require:
      - docker_network: nextcloud1
      - home_salt_states_nextcloud_proxy_container_running

home_salt_states_nextcloud_web_container_running:
  docker_container.running:
    - name: nextcloud-web
    - image: nginx
    - restart: always
    - binds:
      - /srv/docker/nextcloud:/var/www/html:ro
      - /srv/docker/nginx/nextcloud/nginx.conf:/etc/nginx/nginx.conf:ro
    - environment:
      - VIRTUAL_HOST: {{ virtual_host }}
      - LETSENCRYPT_HOST: {{ virtual_host }}
      - LETSENCRYPT_EMAIL:  {{ email }}
    - networks:
      - nextcloud1
    - require:
      - docker_network: nextcloud1
      - home_salt_states_nextcloud_nextcloud_container_running
      - home_salt_states_nextcloud_proxy_container_running
      - home_salt_states_nextcloud_letsencrypt_companion_container_running
      - home_salt_states_nextcloud_nginx_config_managed
    - watch:
      - home_salt_states_nextcloud_nginx_config_managed
