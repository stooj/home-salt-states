{% from "docker_containers/map.jinja" import docker_containers with context %}

{% set mountpoint = docker_containers.mountpoint %}
{% set root_password = docker_containers.mariadb.root_password %}
{% set user_password = docker_containers.mariadb.user_password %}
{% set username = docker_containers.mariadb.username %}

home_salt_states_mariadb_container_running:
  docker_container.running:
    - name: mariadb
    - image: linuxserver/mariadb:latest
    - restart: unless-stopped
    - binds:
      - {{ mountpoint }}/docker/mariadb/config:/config
    - port_bindings:
      - 3306:3306
    - environment:
      - PUID: 1000
      - PGID: 100
      - TZ: Europe/London
      - MYSQL_ROOT_PASSWORD: {{ root_password }}
      - MYSQL_USER: {{ username }}
      - MYSQL_PASSWORD: {{ user_password }}

{% for dir in [
  'docker/mariadb/config',
  ]
%}
home_salt_states_mariadb_{{ dir|replace('/', '_') }}_managed:
  file.directory:
    - name: {{ mountpoint }}/{{ dir }}
    - user: stooj
    - group: users
    - dir_mode: 0755
    - file_mode: 0644
    - makedirs: True
    - recurse:
      - mode
      - user
      - group
{% endfor %}
