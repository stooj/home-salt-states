{% from "docker_containers/map.jinja" import docker_containers with context %}

{% set mountpoint = docker_containers.mountpoint %}

home_salt_states_haproxy_config_file_managed:
  file.managed:
    - name: {{ mountpoint }}/docker/haproxy/config/haproxy.cfg
    - source: salt://docker_containers/files/haproxy/haproxy.cfg
    - user: stooj
    - group: users
    - mode: 644
    - makedirs: True

home_salt_states_haproxy_container_running:
  docker_container.running:
    - name: haproxy
    - image: haproxy:1.9
    - restart: unless-stopped
    - binds:
      - {{ mountpoint }}/docker/haproxy/config:/usr/local/etc/haproxy:ro
    - port_bindings:
      - 80:80
      - 443:443
    - require:
      - home_salt_states_haproxy_config_file_managed
