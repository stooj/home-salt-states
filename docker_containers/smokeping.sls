{% from "docker_containers/map.jinja" import docker_containers with context %}

{% set mountpoint = docker_containers.mountpoint %}

home_salt_states_smokeping_container_running:
  docker_container.running:
    - name: smokeping
    - image: linuxserver/smokeping:latest
    - restart: unless-stopped
    - binds:
      - {{ mountpoint }}/docker/smokeping/config:/config
      - {{ mountpoint }}/docker/smokeping/data:/data
    - port_bindings:
      - 4060:80
    - environment:
      - PUID: 1000
      - PGID: 100
      - TZ: Europe/London
    - labels:
      - "traefik.enable=true"
      - "traefik.http.middlewares.smokeping-redirect-websecure.redirectscheme.scheme=https"
      - "traefik.http.routers.smokeping-web.middlewares=smokeping-redirect-websecure"
      - "traefik.http.routers.smokeping-web.rule=Host(`smokeping.ginstoo.net`)"
      - "traefik.http.routers.smokeping-web.entrypoints=web"
      - "traefik.http.routers.smokeping-websecure.rule=Host(`smokeping.ginstoo.net`)"
      - "traefik.http.routers.smokeping-websecure.tls.certresolver=letsencrypt"
      - "traefik.http.routers.smokeping-websecure.tls=true"
      - "traefik.http.routers.smokeping-websecure.entrypoints=websecure"

{% for dir in [
  'docker/smokeping/config',
  'docker/smokeping/data',
  ]
%}
home_salt_states_smokeping_{{ dir|replace('/', '_') }}_managed:
  file.directory:
    - name: {{ mountpoint }}/{{ dir }}
    - user: stooj
    - group: users
    - dir_mode: 0755
    - file_mode: 0644
    - makedirs: True
    - recurse:
      - mode
      - user
      - group
{% endfor %}
