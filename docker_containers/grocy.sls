{% from "docker_containers/map.jinja" import docker_containers with context %}

{% set mountpoint = docker_containers.mountpoint %}

home_salt_states_grocy_container_running:
  docker_container.running:
    - name: grocy
    - image: linuxserver/grocy:latest
    - restart: unless-stopped
    - binds:
      - {{ mountpoint }}/docker/grocy/config:/config
    - port_bindings:
      - 9283:80
    - environment:
      - PUID: 1000
      - PGID: 100
      - TZ: Europe/London
    - labels:
      - "traefik.enable=true"
      - "traefik.http.middlewares.grocy-redirect-websecure.redirectscheme.scheme=https"
      - "traefik.http.routers.grocy-web.middlewares=grocy-redirect-websecure"
      - "traefik.http.routers.grocy-web.rule=Host(`grocy.ginstoo.net`)"
      - "traefik.http.routers.grocy-web.entrypoints=web"
      - "traefik.http.routers.grocy-websecure.rule=Host(`grocy.ginstoo.net`)"
      - "traefik.http.routers.grocy-websecure.tls.certresolver=letsencrypt"
      - "traefik.http.routers.grocy-websecure.tls=true"
      - "traefik.http.routers.grocy-websecure.entrypoints=websecure"

{% for dir in [
  'docker/grocy/config',
  ]
%}
home_salt_states_grocy_{{ dir|replace('/', '_') }}_managed:
  file.directory:
    - name: {{ mountpoint }}/{{ dir }}
    - user: stooj
    - group: users
    - dir_mode: 0755
    - file_mode: 0644
    - makedirs: True
    - recurse:
      - mode
      - user
      - group
{% endfor %}
