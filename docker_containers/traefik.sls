{% from "docker_containers/map.jinja" import docker_containers with context %}

{% set mountpoint = docker_containers.mountpoint %}
{% set do_auth_token = docker_containers.get('do_auth_token', '12345') %}
{% set letsencrypt_email = docker_containers.get('letsencrypt_email', 'test@example.com') %}
{%- set hostname = salt.grains.get('id', None) %}

home_salt_states_traefik_container_running:
  docker_container.running:
    - name: traefik
    - image: traefik:v2.2.1
    - restart: always
    - binds:
      - {{ mountpoint }}/docker/traefik/config:/etc/traefik/
      - {{ mountpoint }}/docker/traefik/certificates:/letsencrypt
      - /var/run/docker.sock:/var/run/docker.sock
    - port_bindings:
      - 8080:8080
      - 80:80
      - 443:443
    - watch:
      - home_salt_states_traefik_global_config_managed
      {% if hostname == 'avasarala.ginstoo.net' %}
      - home_salt_states_traefik_pris_config_managed
      {% endif %}
    - environment:
      - PUID: 1000
      - PGID: 100
      - TZ: Europe/London
      - DO_AUTH_TOKEN: {{ do_auth_token }}

{% for dir in [
  'docker/traefik/config',
  'docker/traefik/letsencrypt'
  ]
%}
home_salt_states_traefik_{{ dir|replace('/', '_') }}_managed:
  file.directory:
    - name: {{ mountpoint }}/{{ dir }}
    - user: stooj
    - group: users
    - dir_mode: 0755
    - file_mode: 0644
    - makedirs: True
    - recurse:
      - mode
      - user
      - group
{% endfor %}

home_salt_states_traefik_global_config_managed:
  file.managed:
    - name: {{ mountpoint }}/docker/traefik/config/traefik.yaml
    - source: salt://docker_containers/templates/traefik/traefik.yaml.jinja
    - user: stooj
    - group: users
    - mode: 644
    - makedirs: True
    - template: jinja
    - context:
        letsencrypt_email: {{ letsencrypt_email }}

{% if hostname == 'avasarala.ginstoo.net' %}
home_salt_states_traefik_pris_config_managed:
  file.managed:
    - name: {{ mountpoint }}/docker/traefik/config/pris.yaml
    - source: salt://docker_containers/templates/traefik/pris.yaml.jinja
    - user: stooj
    - group: users
    - mode: 644
    - makedirs: True
    - template: jinja
{% endif %}
