{% from "docker_containers/map.jinja" import docker_containers with context %}

{% set mountpoint = docker_containers.mountpoint %}

{% set domain = docker_containers.domain_names.znc %}

home_salt_states_znc_jq_package_installed:
  pkg.installed:
    - pkgs: {{ docker_containers.pkgs.jq|yaml }}

home_salt_states_znc_extract_ssl_script_installed:
  file.managed:
    - name: /usr/sbin/extract_znc_letsencrypt_cert
    - source: salt://docker_containers/templates/znc/extract_ssl_cert.jinja
    - template: jinja
    - context:
        domain: {{ domain }}
        traefik_mountpoint: {{ mountpoint }}/docker/traefik
        znc_pemfile: {{ mountpoint }}/docker/znc/config/znc.pem
        docker_user: stooj
        docker_group: users
    - user: root
    - group: root
    - mode: 700
    - makedirs: True
    - require:
      - home_salt_states_znc_jq_package_installed

home_salt_states_znc_extract_ssl_script_cronjob_managed:
  cron.present:
    - name: /usr/sbin/extract_znc_letsencrypt_cert
    - user: root
    - special: '@daily'
    - require:
      - home_salt_states_znc_extract_ssl_script_installed

home_salt_states_znc_container_running:
  docker_container.running:
    - name: znc
    - image: linuxserver/znc:latest
    - restart: unless-stopped
    - binds:
      - {{ mountpoint }}/docker/znc/config:/config
    - port_bindings:
      - 6501:6501
      - 6502:6502
    - environment:
      - PUID: 1000
      - PGID: 100
      - TZ: Europe/London
    - labels:
      - "traefik.enable=true"
      - "traefik.http.middlewares.znc-redirect-websecure.redirectscheme.scheme=https"
      - "traefik.http.routers.znc-web.middlewares=znc-redirect-websecure"
      - "traefik.http.routers.znc-web.rule=Host(`{{ domain }}`)"
      - "traefik.http.routers.znc-web.entrypoints=web"
      - "traefik.http.routers.znc-websecure.rule=Host(`{{ domain }}`)"
      - "traefik.http.routers.znc-websecure.tls.certresolver=letsencrypt"
      - "traefik.http.routers.znc-websecure.tls=true"
      - "traefik.http.routers.znc-websecure.entrypoints=websecure"

{% for dir in [
  'docker/znc/config',
  ]
%}
home_salt_states_znc_{{ dir|replace('/', '_') }}_managed:
  file.directory:
    - name: {{ mountpoint }}/{{ dir }}
    - user: stooj
    - group: users
    - dir_mode: 0755
    - file_mode: 0644
    - makedirs: True
{% endfor %}
