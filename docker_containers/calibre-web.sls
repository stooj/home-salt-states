{% from "docker_containers/map.jinja" import docker_containers with context %}

{% set mountpoint = docker_containers.mountpoint %}

{% set domain = docker_containers.domain_names.calibre_web %}

home_salt_states_calibre_web_container_running:
  docker_container.running:
    - name: calibre-web
    - image: linuxserver/calibre-web:latest
    - restart: always
    - binds:
      - {{ mountpoint }}/docker/calibre-web/config:/config
      - {{ mountpoint }}/shares/books:/books
    - port_bindings:
      - 8083:8083
    - environment:
      - PUID: 1000
      - PGID: 100
      - TZ: Europe/London
    - labels:
      - "traefik.enable=true"
      - "traefik.http.middlewares.calibre-web-redirect-websecure.redirectscheme.scheme=https"
      - "traefik.http.routers.calibre-web-web.middlewares=calibre-web-redirect-websecure"
      - "traefik.http.routers.calibre-web-web.rule=Host(`{{ domain }}`)"
      - "traefik.http.routers.calibre-web-web.entrypoints=web"
      - "traefik.http.routers.calibre-web-websecure.rule=Host(`{{ domain }}`)"
      - "traefik.http.routers.calibre-web-websecure.tls.certresolver=letsencrypt"
      - "traefik.http.routers.calibre-web-websecure.tls=true"
      - "traefik.http.routers.calibre-web-websecure.entrypoints=websecure"

{% for dir in [
  'docker/calibre-web/config',
  'shares/books',
  ]
%}
home_salt_states_calibre_web_{{ dir|replace('/', '_') }}_managed:
  file.directory:
    - name: {{ mountpoint }}/{{ dir }}
    - user: stooj
    - group: users
    - dir_mode: 0755
    - file_mode: 0644
    - makedirs: True
{% endfor %}
