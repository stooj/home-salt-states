{% from "docker_containers/map.jinja" import docker_containers with context %}

{% set mountpoint = docker_containers.mountpoint %}

{% if docker_containers.beets is defined %}
{% set apikey = docker_containers.beets.get('acoustid_apikey', "NOKEY") %}
{% else %}
{% set apikey = "NOKEY" %}
{% endif %}
home_salt_states_beets_config_managed:
  file.managed:
    - name: {{ mountpoint }}/docker/beets/config/config.yaml
    - source: salt://docker_containers/files/beets/config.yaml.jinja
    - template: jinja
    - context:
        apikey: {{ apikey }}
    - user: stooj
    - group: users
    - mode: 644
    - makedirs: True

home_salt_states_beets_container_running:
  docker_container.running:
    - name: beets
    - image: linuxserver/beets:latest
    - restart: always
    - binds:
      - {{ mountpoint }}/docker/beets/config:/config
      - {{ mountpoint }}/shares/audio/music:/music
      - {{ mountpoint }}/shares/audio/incoming_music:/downloads
    - port_bindings:
      - 8337:8337
    - environment:
      - PUID: 1000
      - PGID: 100
      - TZ: Europe/London

{% for dir in [
  'docker/beets/config',
  'shares/audio/music',
  'shares/audio/incoming_music'
  ]
%}
home_salt_states_beets_{{ dir|replace('/', '_') }}_managed:
  file.directory:
    - name: {{ mountpoint }}/{{ dir }}
    - user: stooj
    - group: users
    - dir_mode: 0755
    - file_mode: 0644
    - makedirs: True
    - recurse:
      - mode
      - user
      - group
    - force: true
{% endfor %}
