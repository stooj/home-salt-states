{% from "docker_containers/map.jinja" import docker_containers with context %}

{% set mountpoint = docker_containers.mountpoint %}

{% set domain = docker_containers.domain_names.home_assistant %}

home_salt_states_home_assistant_container_running:
  docker_container.running:
    - name: home-assistant
    - image: homeassistant/home-assistant:stable
    - restart: unless-stopped
    - binds:
      - {{ mountpoint }}/docker/home-assistant/config:/config
    - port_bindings:
      - 8123:8123
    - environment:
      - TZ: Europe/London
    - labels:
      - "traefik.enable=true"
      - "traefik.http.middlewares.home-assistant-redirect-websecure.redirectscheme.scheme=https"
      - "traefik.http.routers.home-assistant-web.middlewares=home-assistant-redirect-websecure"
      - "traefik.http.routers.home-assistant-web.rule=Host(`{{ domain }}`)"
      - "traefik.http.routers.home-assistant-web.entrypoints=web"
      - "traefik.http.routers.home-assistant-websecure.rule=Host(`{{ domain }}`)"
      - "traefik.http.routers.home-assistant-websecure.tls.certresolver=letsencrypt"
      - "traefik.http.routers.home-assistant-websecure.tls=true"
      - "traefik.http.routers.home-assistant-websecure.entrypoints=websecure"

{% for dir in [
  'docker/home-assistant/config',
  ]
%}
home_salt_states_home_assistant_{{ dir|replace('/', '_') }}_managed:
  file.directory:
    - name: {{ mountpoint }}/{{ dir }}
    - user: stooj
    - group: users
    - dir_mode: 0755
    - file_mode: 0644
    - makedirs: True
{% endfor %}
