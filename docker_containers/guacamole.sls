{% from "docker_containers/map.jinja" import docker_containers with context %}

{% set mountpoint = docker_containers.mountpoint %}

home_salt_states_gaucd_container_running:
  docker_container.running:
    - name: guacd
    - image: guacamole/guacd:latest
    - restart: unless-stopped

home_salt_states_guacamole_container_running:
  docker_container.running:
    - name: guacamole
    - image: guacamole/guacamole:latest
    - restart: unless-stopped
    - links:
      - guacd: guacd
      - postgres: postgres
    - port_bindings:
      - 8090:8080
    - require:
      - home_salt_states_gaucd_container_running
      - home_salt_states_nextcloud_postgres_container_running
      - home_salt_states_guacamole_database_creation_script_present

home_salt_states_guacamole_database_present:
  postgres_database.present:
    - name: guacamole_db
    - db_port: 5432
    - require:
      - home_salt_states_nextcloud_postgres_container_running

home_salt_states_guacamole_database_creation_script_present:
  cmd.run:
    - name: docker run --rm guacamole/guacamole /opt/guacamole/bin/initdb.sh --postgres > {{ mountpoint }}/docker/guacamole/initdb.sql
    - creates: {{ mountpoint }}/docker/guacamole/initdb.sql
