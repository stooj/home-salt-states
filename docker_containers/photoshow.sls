{% from "docker_containers/map.jinja" import docker_containers with context %}

{% set mountpoint = docker_containers.mountpoint %}

home_salt_states_photoshow_container_running:
  docker_container.running:
    - name: photoshow
    - image: linuxserver/photoshow:latest
    - restart: unless-stopped
    - binds:
      - {{ mountpoint }}/docker/photoshow/config:/config
      - {{ mountpoint }}/shares/pictures:/pictures:ro
      - {{ mountpoint }}/docker/photoshow/thumbs:/Thumbs
    - port_bindings:
      - 4050:80
    - environment:
      - PUID: 1000
      - PGID: 100
      - TZ: Europe/London
    - labels:
      - "traefik.enable=true"
      - "traefik.http.routers.photoshow.rule=Host(`photoshow.ginstoo.net`)"
      - "traefik.http.routers.photoshow.entrypoints=websecure"
      - "traefik.http.routers.photoshow.tls.certresolver=letsencrypt"

{% for dir in [
  'docker/photoshow/config',
  'docker/photoshow/thumbs',
  'shares/pictures',
  ]
%}
home_salt_states_photoshow_{{ dir|replace('/', '_') }}_managed:
  file.directory:
    - name: {{ mountpoint }}/{{ dir }}
    - user: stooj
    - group: users
    - dir_mode: 0755
    - file_mode: 0644
    - makedirs: True
    - recurse:
      - mode
      - user
      - group
{% endfor %}
