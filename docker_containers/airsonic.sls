{% from "docker_containers/map.jinja" import docker_containers with context %}

{% set mountpoint = docker_containers.mountpoint %}

home_salt_states_airsonic_container_running:
  docker_container.running:
    - name: airsonic
    - image: linuxserver/airsonic:latest
    - restart: always
    - binds:
      - {{ mountpoint }}/docker/airsonic/config:/config
      - {{ mountpoint }}/shares/audio/music:/music
      - {{ mountpoint }}/shares/audio/playlists:/playlists
      - {{ mountpoint }}/shares/audio/podcasts:/podcasts
    - port_bindings:
      - 4040:4040
    - environment:
      - PUID: 1000
      - PGID: 100
      - TZ: Europe/London
    - labels:
      - "traefik.enable=true"
      - "traefik.http.middlewares.airsonic-redirect-websecure.redirectscheme.scheme=https"
      - "traefik.http.routers.airsonic-web.middlewares=airsonic-redirect-websecure"
      - "traefik.http.routers.airsonic-web.rule=Host(`airsonic.ginstoo.net`)"
      - "traefik.http.routers.airsonic-web.entrypoints=web"
      - "traefik.http.routers.airsonic-websecure.rule=Host(`airsonic.ginstoo.net`)"
      - "traefik.http.routers.airsonic-websecure.tls.certresolver=letsencrypt"
      - "traefik.http.routers.airsonic-websecure.tls=true"
      - "traefik.http.routers.airsonic-websecure.entrypoints=websecure"

home_salt_states_airsonic_transcoding_requirements_installed:
  pkg.installed:
    - pkgs: {{ docker_containers.pkgs.airsonic_transcoding|yaml }}

{% for dir in [
  'shares/audio/music',
  'shares/audio/playlists',
  'shares/audio/podcasts'
  ]
%}
home_salt_states_airsonic_{{ dir|replace('/', '_') }}_managed:
  file.directory:
    - name: {{ mountpoint }}/{{ dir }}
    - user: stooj
    - group: users
    - dir_mode: 0755
    - file_mode: 0644
    - makedirs: True
    - recurse:
      - mode
      - user
      - group
{% endfor %}

{% for dir in [
  'docker/airsonic/config',
  ]
%}
home_salt_states_airsonic_{{ dir|replace('/', '_') }}_managed:
  file.directory:
    - name: {{ mountpoint }}/{{ dir }}
    - user: stooj
    - group: users
    - dir_mode: 0755
    - file_mode: 0644
    - makedirs: True
{% endfor %}
