home_salt_states_unifi_docker_container_running:
  docker_container.running:
    - name: unifi-controller
    - image: jacobalberty/unifi:stable
    - restart: always
    - port_bindings:
      - 8080:8080
      - 8443:8443
      - 6789:6789
      - 3478:3478/udp
      - 10001:10001/udp
    - binds:
      - /srv/docker/unifi-controller:/unifi
    - environment:
      - TZ: 'Europe/London'
      - JVM_MAX_THREAD_STACK_SIZE: 1280k
    - require:
      - sls: docker.install
      - home_salt_states_unifi_docker_image_present
