{% from "docker_containers/map.jinja" import docker_containers with context %}

{% set mountpoint = docker_containers.mountpoint %}
{% set domain = docker_containers.domain_names.pelican_blog %}
{% set blog_dir = docker_containers.pelican_blog.blog_dir %}

home_salt_states_blog_nginx_config_managed:
  file.managed:
    - name: {{ mountpoint }}/docker/blog/config/nginx/nginx.conf
    - source: salt://docker_containers/files/blog/nginx.conf
    - user: root
    - group: root
    - mode: 644
    - makedirs: True

home_salt_states_blog_container_running:
  docker_container.running:
    - name: blog
    - image: nginx:alpine
    - restart: always
    - binds:
      - {{ mountpoint }}/docker/blog/config/nginx/nginx.conf:/etc/nginx/nginx.conf:ro
      - {{ blog_dir }}:/var/www/blog:ro
    - port_bindings:
      - 4082:80
    - labels:
      - "traefik.enable=true"
      - "traefik.http.routers.blog-web.rule=Host(`{{ domain }}`)"
      - "traefik.http.routers.blog-web.entrypoints=web"
      - "traefik.http.routers.blog-web.middlewares=blog-redirect-websecure"
      - "traefik.http.middlewares.blog-redirect-websecure.redirectscheme.scheme=https"
      - "traefik.http.routers.blog-websecure.rule=Host(`{{ domain }}`)"
      - "traefik.http.routers.blog-websecure.tls.certresolver=letsencrypt"
      - "traefik.http.routers.blog-websecure.tls=true"
      - "traefik.http.routers.blog-websecure.entrypoints=websecure"
    - watch:
      - home_salt_states_blog_nginx_config_managed

{% for dir in [
  blog_dir,
  ]
%}
home_salt_states_blog_{{ dir|replace('/', '_') }}_managed:
  file.directory:
    - name: {{ dir }}
    - user: stooj
    - group: users
    - dir_mode: 0755
    - file_mode: 0644
    - makedirs: True
    - recurse:
      - mode
      - user
      - group
{% endfor %}
