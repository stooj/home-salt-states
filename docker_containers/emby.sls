{% from "docker_containers/map.jinja" import docker_containers with context %}

{% set mountpoint = docker_containers.mountpoint %}

home_salt_states_emby_container_running:
  docker_container.running:
    - name: emby
    - image: linuxserver/emby:latest
    - restart: always
    - binds:
      - {{ mountpoint }}/docker/emby/config:/config
      - {{ mountpoint }}/shares/media/tv:/data/tvshows
      - {{ mountpoint }}/shares/media/movies:/data/movies
      - {{ mountpoint }}/shares/audio/audiobooks:/data/audiobooks
      - {{ mountpoint }}/shares/audio/music:/data/music
    - port_bindings:
      - 8196:8096
    - environment:
      - PUID: 1000
      - PGID: 100
      - TZ: Europe/London
    - labels:
      - "traefik.enable=true"
      - "traefik.http.middlewares.emby-redirect-websecure.redirectscheme.scheme=https"
      - "traefik.http.routers.emby-web.middlewares=emby-redirect-websecure"
      - "traefik.http.routers.emby-web.rule=Host(`emby.ginstoo.net`)"
      - "traefik.http.routers.emby-web.entrypoints=web"
      - "traefik.http.routers.emby-websecure.rule=Host(`emby.ginstoo.net`)"
      - "traefik.http.routers.emby-websecure.tls.certresolver=letsencrypt"
      - "traefik.http.routers.emby-websecure.tls=true"
      - "traefik.http.routers.emby-websecure.entrypoints=websecure"

{% for dir in [
  'docker/emby/config',
  'shares/media/tv',
  'shares/media/movies',
  'shares/audio/audiobooks',
  'shares/audio/music'
  ]
%}
home_salt_states_emby_{{ dir|replace('/', '_') }}_managed:
  file.directory:
    - name: {{ mountpoint }}/{{ dir }}
    - user: stooj
    - group: users
    - dir_mode: 0755
    - file_mode: 0644
    - makedirs: True
    - recurse:
      - mode
      - user
      - group
{% endfor %}

home_salt_states_emby_max_user_watches_updated:
  sysctl.present:
    - name: fs.inotify.max_user_watches
    - value: 524288
