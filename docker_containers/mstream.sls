{% from "docker_containers/map.jinja" import docker_containers with context %}

{% set mountpoint = docker_containers.mountpoint %}

{% set domain = docker_containers.domain_names.mstream %}
{% set password = docker_containers.mstream.password %}
{% set user = docker_containers.mstream.username %}

home_salt_states_mstream_container_running:
  docker_container.running:
    - name: mstream
    - image: linuxserver/mstream:latest
    - restart: unless-stopped
    - binds:
      - {{ mountpoint }}/docker/mstream/config:/config
      - {{ mountpoint }}/shares/audio/music:/music
    - port_bindings:
      - 3000:3000
    - environment:
      - PUID: 1000
      - PGID: 100
      - TZ: Europe/London
      - USER: {{ user }}
      - PASSWORD: {{ password }}
      - USE_JSON: false
    - labels:
      - "traefik.enable=true"
      - "traefik.http.middlewares.mstream-redirect-websecure.redirectscheme.scheme=https"
      - "traefik.http.routers.mstream-web.middlewares=mstream-redirect-websecure"
      - "traefik.http.routers.mstream-web.rule=Host(`{{ domain }}`)"
      - "traefik.http.routers.mstream-web.entrypoints=web"
      - "traefik.http.routers.mstream-websecure.rule=Host(`{{ domain }}`)"
      - "traefik.http.routers.mstream-websecure.tls.certresolver=letsencrypt"
      - "traefik.http.routers.mstream-websecure.tls=true"
      - "traefik.http.routers.mstream-websecure.entrypoints=websecure"

{% for dir in [
  'docker/mstream/config',
  'shares/audio/music'
  ]
%}
home_salt_states_mstream_{{ dir|replace('/', '_') }}_managed:
  file.directory:
    - name: {{ mountpoint }}/{{ dir }}
    - user: stooj
    - group: users
    - dir_mode: 0755
    - file_mode: 0644
    - makedirs: True
    - recurse:
      - mode
      - user
      - group
{% endfor %}
