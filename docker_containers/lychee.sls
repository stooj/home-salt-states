{% from "docker_containers/map.jinja" import docker_containers with context %}

{% set mountpoint = docker_containers.mountpoint %}

home_salt_states_lychee_user_config_managed:
  file.managed:
    - name: {{ mountpoint }}/docker/lychee/config/lychee/user.ini
    - source: salt://docker_containers/files/lychee/user.ini
    - user: stooj
    - group: users
    - mode: 644
    - makedirs: True

home_salt_states_lychee_container_running:
  docker_container.running:
    - name: lychee
    - image: linuxserver/lychee:latest
    - restart: unless-stopped
    - binds:
      - {{ mountpoint }}/docker/lychee/config:/config
      - {{ mountpoint }}/shares/pictures/lychee:/pictures
    - port_bindings:
      - 4050:80
      - 4051:443
    - environment:
      - PUID: 1000
      - PGID: 100
      - TZ: Europe/London
    - links:
      - mariadb: mariadb
    - labels:
      - "traefik.enable=true"
      - "traefik.http.middlewares.lychee-redirect-websecure.redirectscheme.scheme=https"
      - "traefik.http.routers.lychee-web.middlewares=lychee-redirect-websecure"
      - "traefik.http.routers.lychee-web.rule=Host(`lychee.ginstoo.net`)"
      - "traefik.http.routers.lychee-web.entrypoints=web"
      - "traefik.http.routers.lychee-websecure.rule=Host(`lychee.ginstoo.net`)"
      - "traefik.http.routers.lychee-websecure.tls.certresolver=letsencrypt"
      - "traefik.http.routers.lychee-websecure.tls=true"
      - "traefik.http.routers.lychee-websecure.entrypoints=websecure"
    - watch:
      - home_salt_states_lychee_user_config_managed

{% for dir in [
  'docker/lychee/config',
  'shares/pictures/lychee',
  ]
%}
home_salt_states_lychee_{{ dir|replace('/', '_') }}_managed:
  file.directory:
    - name: {{ mountpoint }}/{{ dir }}
    - user: stooj
    - group: users
    - dir_mode: 0755
    - file_mode: 0644
    - makedirs: True
    - recurse:
      - mode
      - user
      - group
{% endfor %}
