home_salt_states_nginx_proxy_docker_image_present:
  docker_image.present:
    - name: jwilder/nginx-proxy
    - tag: alpine
    - force: true
    - require:
      - sls: docker.install

home_salt_states_nginx_proxy_container_running:
  docker_container.running:
    - name: nginx-proxy
    - image: jwilder/nginx-proxy
    - restart: unless-stopped
    - binds:
      - /etc/nginx/certs
      - /etc/nginx/vhost.d
      - /usr/share/nginx/html
      - /var/run/docker.sock:/tmp/docker.sock:ro
    - port_bindings:
      - 9080:80
      - 9090:443
    - environment:
      - TZ: Europe/London
