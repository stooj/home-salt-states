{% from "docker_containers/map.jinja" import docker_containers with context %}

{% set mountpoint = docker_containers.mountpoint %}

{% set domain = docker_containers.domain_names.mosquitto %}

home_salt_states_mosquitto_jq_package_installed:
  pkg.installed:
    - pkgs: {{ docker_containers.pkgs.jq|yaml }}

{#
home_salt_states_mosquitto_extract_ssl_script_installed:
  file.managed:
    - name: /usr/sbin/extract_mosquitto_letsencrypt_cert
    - source: salt://docker_containers/templates/mosquitto/extract_ssl_cert.jinja
    - template: jinja
    - context:
        domain: {{ domain }}
        traefik_mountpoint: {{ mountpoint }}/docker/traefik
        mosquitto_pemfile: {{ mountpoint }}/docker/mosquitto/config/mosquitto.pem
        docker_user: stooj
        docker_group: users
    - user: root
    - group: root
    - mode: 700
    - makedirs: True
    - require:
      - home_salt_states_mosquitto_jq_package_installed
#}

{#
home_salt_states_mosquitto_extract_ssl_script_cronjob_managed:
  cron.present:
    - name: /usr/sbin/extract_mosquitto_letsencrypt_cert
    - user: root
    - special: '@daily'
    - require:
      - home_salt_states_mosquitto_extract_ssl_script_installed
#}

home_salt_states_mosquitto_container_running:
  docker_container.running:
    - name: mosquitto
    - image: eclipse-mosquitto:1.6
    - restart: unless-stopped
    - binds:
      - {{ mountpoint }}/docker/mosquitto/config:/config
      - {{ mountpoint }}/docker/mosquitto/data:/data
      - {{ mountpoint }}/docker/mosquitto/log:/log
    - port_bindings:
      - 1883:1883
      - 9001:9001
    - labels:
      - "traefik.enable=true"
      {#
      - "traefik.http.middlewares.mosquitto-redirect-websecure.redirectscheme.scheme=https"
      - "traefik.http.routers.mosquitto-web.middlewares=mosquitto-redirect-websecure"
      - "traefik.http.routers.mosquitto-web.rule=Host(`{{ domain }}`)"
      - "traefik.http.routers.mosquitto-web.entrypoints=web"
      #}
      - "traefik.http.routers.mosquitto-websecure.rule=Host(`{{ domain }}`)"
      - "traefik.http.routers.mosquitto-websecure.tls.certresolver=letsencrypt"
      - "traefik.http.routers.mosquitto-websecure.tls=true"
      - "traefik.http.routers.mosquitto-websecure.entrypoints=websecure"

{% for dir in [
  'docker/mosquitto/config',
  'docker/mosquitto/data',
  'docker/mosquitto/log',
  ]
%}
home_salt_states_mosquitto_{{ dir|replace('/', '_') }}_managed:
  file.directory:
    - name: {{ mountpoint }}/{{ dir }}
    - user: stooj
    - group: users
    - dir_mode: 0755
    - file_mode: 0644
    - makedirs: True
{% endfor %}
