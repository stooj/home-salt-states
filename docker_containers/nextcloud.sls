{% from "docker_containers/map.jinja" import docker_containers with context %}

{% set mountpoint = docker_containers.mountpoint %}

{% set postgres_user_name = docker_containers.postgres.nextcloud.user_name %}
{% set postgres_password = docker_containers.postgres.nextcloud.user_password %}
{% set hostname = docker_containers.domain_names.nextcloud %}

home_salt_states_nextcloud_postgres_container_running:
  docker_container.running:
    - name: postgres
    - image: postgres:alpine
    - restart: always
    - binds:
      - {{ mountpoint }}/docker/postgres/db:/var/lib/postgresql/data
    - environment:
      - POSTGRES_PASSWORD: {{ postgres_password }}
      - POSTGRES_DB: nextcloud
      - POSTGRES_USER: {{ postgres_user_name }}

home_salt_states_nextcloud_nextcloud_container_running:
  docker_container.running:
    - name: nextcloud
    - image: nextcloud:fpm
    - restart: always
    - binds:
      - {{ mountpoint }}/docker/nextcloud/html:/var/www/html
      - {{ mountpoint }}/nextcloud/data:/var/www/html/data
      - {{ mountpoint }}/docker/nextcloud/config:/var/www/html/config
    - environment:
      - POSTGRES_HOST: postgres
      - POSTGRES_PASSWORD: {{ postgres_password }}
      - POSTGRES_DB: nextcloud
      - POSTGRES_USER: {{ postgres_user_name }}
    - links:
      - postgres: postgres
    - require:
      - home_salt_states_nextcloud_postgres_container_running

home_salt_states_nextcloud_nginx_config_managed:
  file.managed:
    - name: {{ mountpoint }}/docker/nginx/nextcloud/nginx.conf
    - source: salt://docker_containers/files/nextcloud/nginx/nginx.conf
    - user: root
    - group: root
    - mode: 644
    - makedirs: True
    - require:
      - home_salt_states_nextcloud_postgres_container_running

home_salt_states_nextcloud_nginx_container_running:
  docker_container.running:
    - name: nextcloud-web
    - image: nginx:alpine
    - restart: always
    - port_bindings:
      - 9010:80
    - binds:
      - {{ mountpoint }}/docker/nextcloud/html:/var/www/html:ro
      - {{ mountpoint }}/docker/nginx/nextcloud/nginx.conf:/etc/nginx/nginx.conf:ro
    - links:
      - nextcloud: nextcloud
    - watch:
      - home_salt_states_nextcloud_nginx_config_managed
    - labels:
      - "traefik.enable=true"
      - "traefik.http.middlewares.nextcloud-redirect-websecure.redirectscheme.scheme=https"
      - "traefik.http.routers.nextcloud-web.middlewares=nextcloud-redirect-websecure"
      - "traefik.http.routers.nextcloud-web.rule=Host(`nextcloud.ginstoo.net`)"
      - "traefik.http.routers.nextcloud-web.entrypoints=web"
      - "traefik.http.routers.nextcloud-websecure.rule=Host(`nextcloud.ginstoo.net`)"
      - "traefik.http.routers.nextcloud-websecure.tls.certresolver=letsencrypt"
      - "traefik.http.routers.nextcloud-websecure.tls=true"
      - "traefik.http.routers.nextcloud-websecure.entrypoints=websecure"
    - require:
      - home_salt_states_nextcloud_nextcloud_container_running
      - home_salt_states_nextcloud_nginx_config_managed

home_salt_states_nextcloud_nextcloud_config_trusted_proxies_added:
  file.line:
    - name: {{ mountpoint }}/docker/nextcloud/config/config.php
    - content: "  'trusted_proxies' => array ('192.168.0.0/24'),"
    - mode: ensure
    - before: "  'datadirectory' => '/var/www/html/data',"
    - require:
      - home_salt_states_nextcloud_nextcloud_container_running

home_salt_states_nextcloud_nextcloud_config_forwarded_for_headers_added:
  file.line:
    - name: {{ mountpoint }}/docker/nextcloud/config/config.php
    - content: "  'forwarded_for_headers' => array ('HTTP_CF_CONNECTING_IP'),"
    - mode: ensure
    - after: "  'datadirectory' => '/var/www/html/data',"
    - require:
      - home_salt_states_nextcloud_nextcloud_container_running

home_salt_states_nextcloud_nextcloud_config_overwrite_cli_url_added:
  file.line:
    - name: {{ mountpoint }}/docker/nextcloud/config/config.php
    - content: "  'overwrite.cli.url' => 'https://{{ hostname }}',"
    - mode: ensure
    - before: "  'dbname' => 'nextcloud',"
    - require:
      - home_salt_states_nextcloud_nextcloud_container_running

home_salt_states_nextcloud_nextcloud_config_overwrite_protocol_added:
  file.line:
    - name: {{ mountpoint }}/docker/nextcloud/config/config.php
    - content: "  'overwriteprotocol' => 'https',"
    - mode: ensure
    - after: "  'dbname' => 'nextcloud',"
    - require:
      - home_salt_states_nextcloud_nextcloud_container_running
