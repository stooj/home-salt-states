{% from "docker_containers/map.jinja" import docker_containers with context %}

home_salt_states_nginx_proxy_companion_docker_image_present:
  docker_image.present:
    - name: jrcs/letsencrypt-nginx-proxy-companion
    - force: true
    - tag: latest
    - require:
      - sls: docker.install

{% set letsencrypt_email = docker_containers.get('letsencrypt_email', 'test@example.com') %}
home_salt_states_nginx_proxy_companion_container_running:
  docker_container.running:
    - name: letsencrypt-companion
    - image: jrcs/letsencrypt-nginx-proxy-companion
    - restart: unless-stopped
    - volumes_from: nginx-proxy
    - binds:
      - /var/run/docker.sock:/var/run/docker.sock:ro
    - environment:
      - DEFAULT_EMAIL: {{ letsencrypt_email }}
