{% from "docker_containers/map.jinja" import docker_containers with context %}

{% set mountpoint = docker_containers.mountpoint %}

home_salt_states_jellyfin_container_running:
  docker_container.running:
    - name: jellyfin
    - image: linuxserver/jellyfin:latest
    - restart: unless-stopped
    - binds:
      - {{ mountpoint }}/docker/jellyfin/config:/config
      - {{ mountpoint }}/shares/media/tv:/data/tvshows
      - {{ mountpoint }}/shares/media/movies:/data/movies
    - port_bindings:
      - 8096:8096
    - environment:
      - PUID: 1000
      - PGID: 100
      - TZ: Europe/London
    - labels:
      - "traefik.enable=true"
      - "traefik.http.middlewares.jellyfin-redirect-websecure.redirectscheme.scheme=https"
      - "traefik.http.routers.jellyfin-web.middlewares=jellyfin-redirect-websecure"
      - "traefik.http.routers.jellyfin-web.rule=Host(`jellyfin.ginstoo.net`)"
      - "traefik.http.routers.jellyfin-web.entrypoints=web"
      - "traefik.http.routers.jellyfin-websecure.rule=Host(`jellyfin.ginstoo.net`)"
      - "traefik.http.routers.jellyfin-websecure.tls.certresolver=letsencrypt"
      - "traefik.http.routers.jellyfin-websecure.tls=true"
      - "traefik.http.routers.jellyfin-websecure.entrypoints=websecure"

{% for dir in [
  'docker/jellyfin/config',
  'shares/media/tv',
  'shares/media/movies'
  ]
%}
home_salt_states_jellyfin_{{ dir|replace('/', '_') }}_managed:
  file.directory:
    - name: {{ mountpoint }}/{{ dir }}
    - user: stooj
    - group: users
    - dir_mode: 0755
    - file_mode: 0644
    - makedirs: True
    - recurse:
      - mode
      - user
      - group
{% endfor %}
