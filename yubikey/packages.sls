{% from 'yubikey/map.jinja' import requirements with context %}

{% for requirement, package in requirements.pkgs.items() %}
yubikey_{{ requirement }}_installed:
  pkg.installed:
    - name: {{ package }}
{% endfor %}

{% set os_family = salt.grains.get('os_family', None) %}
{% if os_family == 'Arch' %}
{# Arch doesn't package the required gnupg package any more. Install it with pip #}
yubikey_pip_gnupg_installed:
  pip.installed:
    - name: gnupg
    - bin_env: /usr/bin/pip2
{% endif %}
