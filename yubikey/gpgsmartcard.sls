{% from 'yubikey/map.jinja' import requirements with context %}

yubikey_gpg_smartcard_pcscd_service_running:
  service.running:
    - enable: True
    - name: pcscd
{% if salt['grains.get']('os_family') == 'Arch' %}
    - require:
      - pkg: yubikey_pcsc_tools_installed
{% endif %}

{%- for username, keys in salt['pillar.get']('yubikey:gpgsmartcard', {}).items() %}
{% if keys is defined %}
{% for key in keys %}
yubikey_gpg_smartcard_{{ username }}_{{ key }}_added:
  gpg.present:
    - name: {{ key }}
    - keys: {{ key }}
    - keyserver: hkps://hkps.pool.sks-keyservers.net
    - user: {{ username }}
    - trust: ultimately
    - require:
      - user: {{ username }}
      - pkg: yubikey_python_gnupg_installed
{% endfor %}
{% endif %}
{% endfor %}
