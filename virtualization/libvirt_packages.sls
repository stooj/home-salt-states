{% from "virtualization/map.jinja" import virtualization with context %}

{% if salt['grains.get']('os_family', None) == 'Arch' %}
home_salt_states_virtualization_networking_installed:
  pkg.installed:
    - pkgs: {{ virtualization.pkgs.networking|yaml }}

{# See https://wiki.archlinux.org/index.php/Libvirt

 # > Note: As of libvirt 4.8.0 you need to change the firewall backend in
 # >/etc/firewalld/firewalld.conf from nftables to iptables.
 #}
home_salt_states_virtualization_networking_firewall_backend_configured:
  file.replace:
    - name: /etc/firewalld/firewalld.conf
    - pattern: 'FirewallBackend=nftables'
    - repl: 'FirewallBackend=iptables'
    - require:
      - home_salt_states_virtualization_networking_installed
{% endif %}
