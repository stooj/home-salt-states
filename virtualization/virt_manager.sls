{% from "virtualization/map.jinja" import virtualization with context %}

home_salt_states_virtualization_virt_manager_installed:
  pkg.installed:
    - pkgs: {{ virtualization.pkgs.virt_manager|yaml }}
