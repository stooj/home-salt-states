rxvt-file-to-enable-full-terminal-support-on-all-machines:
  file.managed:
    - source: salt://server_linux/files/usr/share/terminfo/r/rxvt-unicode-256color
    - name: /usr/share/terminfo/r/rxvt-unicode-256color
    - user: root
    - group: root
    - mode: 644
    - makedirs: True
    - show_changes: False  # Workaround for issue 46858
