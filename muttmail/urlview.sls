{% from "muttmail/map.jinja" import muttmail with context %}

{% if salt['grains.get']('os_family', 'None') == 'Debian' %}
home_saltstates_muttmail_urlview_installed:
  pkg.installed:
    - pkgs: {{ muttmail.pkgs.urlview|yaml }}
{% endif %}
