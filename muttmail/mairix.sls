{% from "muttmail/map.jinja" import muttmail with context %}

home_saltstates_muttmail_mairix_installed:
  pkg.installed:
    - pkgs: {{ muttmail.pkgs.mairix|yaml }}
