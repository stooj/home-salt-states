{% from "muttmail/map.jinja" import muttmail with context %}

home_saltstates_muttmail_offlineimap_installed:
  pkg.installed:
    - pkgs: {{ muttmail.pkgs.offlineimap|yaml }}
