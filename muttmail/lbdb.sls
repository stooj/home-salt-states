{% from "muttmail/map.jinja" import muttmail with context %}

{% if salt['grains.get']('os_family', 'None') == 'Debian' %}
home_saltstates_muttmail_lbdb_installed:
  pkg.installed:
    - pkgs: {{ muttmail.pkgs.lbdb|yaml }}
{% endif %}
