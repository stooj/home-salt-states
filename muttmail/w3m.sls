{% from "muttmail/map.jinja" import muttmail with context %}

home_saltstates_muttmail_w3m_installed:
  pkg.installed:
    - pkgs: {{ muttmail.pkgs.w3m|yaml }}
