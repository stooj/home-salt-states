{% from "muttmail/map.jinja" import muttmail with context %}

home_saltstates_muttmail_neomutt_installed:
  pkg.installed:
    - pkgs: {{ muttmail.pkgs.neomutt|yaml }}
