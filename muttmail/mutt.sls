{% from "muttmail/map.jinja" import muttmail with context %}

home_saltstates_muttmail_mutt_installed:
  pkg.installed:
    - pkgs: {{ muttmail.pkgs.mutt|yaml }}
