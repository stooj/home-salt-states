{% from "muttmail/map.jinja" import muttmail with context %}

home_saltstates_muttmail_inotify_installed:
  pkg.installed:
    - pkgs: {{ muttmail.pkgs.inotify|yaml }}
