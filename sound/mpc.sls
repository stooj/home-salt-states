{% from "sound/map.jinja" import sound with context %}

home_salt_states_sound_mpc_installed:
  pkg.installed:
    - pkgs: {{ sound.pkgs.mpc|yaml }}

home_salt_states_sound_ncmpcpp_installed:
  pkg.installed:
    - pkgs: {{ sound.pkgs.ncmpcpp|yaml }}
