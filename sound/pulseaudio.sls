{% from "sound/map.jinja" import sound with context %}

home_salt_states_sound_pulseaudio_installed:
  pkg.installed:
    - pkgs: {{ sound.pkgs.pulseaudio|yaml }}


home_salt_states_sound_pulseaudio_volume_controls_installed:
  pkg.installed:
    - pkgs: {{ sound.pkgs.pulsevolumectl|yaml }}

home_salt_states_rtp_recv_module_enabled:
  file.uncomment:
    - name: /etc/pulse/default.pa
    - regex: load-module module-rtp-recv
