{% from "sound/map.jinja" import sound with context %}

home_salt_states_sound_mpd_installed:
  pkg.installed:
    - pkgs: {{ sound.pkgs.mpd|yaml }}

home_salt_states_sound_mpd_service_disabled:
  service.dead:
    - name: mpd
    - enable: False
home_salt_states_sound_mpd_socket_disabled:
  service.dead:
    - name: mpd.socket
    - enable: False
