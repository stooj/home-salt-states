{% from "sound/map.jinja" import sound with context %}

home_salt_states_sound_beets_installed:
  pkg.installed:
    - pkgs: {{ sound.pkgs.beets|yaml }}

home_salt_states_sound_beets_pylast_installed:
  pkg.installed:
    - pkgs: {{ sound.pkgs.pylast|yaml }}

home_salt_states_sound_beets_acoustid_installed:
  pkg.installed:
    - pkgs: {{ sound.pkgs.acoustid|yaml }}
