{% from "chat/map.jinja" import chat with context %}

home_salt_states_chat_weechat_installed:
  pkg.installed:
    - pkgs: {{ chat.pkgs.weechat|yaml }}
