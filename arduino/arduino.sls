{% from "arduino/map.jinja" import arduino with context %}

home_salt_states_arduino_arduino_installed:
  pkg.installed:
    - pkgs: {{ arduino.pkgs.arduino|yaml }}

{# Disable until https://github.com/saltstack/salt/issues/49967 is really fixed
home_salt_states_arduino_pymata_installed:
  pip.installed:
    - pkgs: {{ arduino.pip_packages.pymata }}
#}

{% set groups = arduino.groups -%}
{%- for group in groups -%}
home_salt_states_arduino_{{ group }}_managed:
  group.present:
    - name: {{ group }}
    - addusers:
      {% for username, user in salt['pillar.get']('users', {}).items() if (user.absent is not defined or not user.absent) %}
      - {{ username }}
      {% endfor %}
{% endfor %}
